package com.com.Utilities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.cn.finalteam.galleryfinal.PhotoChooseActivity;
import com.cn.finalteam.galleryfinal.adapter.FolderListAdapter;
import com.cn.finalteam.galleryfinal.model.PhotoFolderInfo;
import com.imagezoomcrop.cropoverlay.CropOverlayView;
import com.imagezoomcrop.cropoverlay.edge.Edge;
import com.imagezoomcrop.demo.Utils;
import com.imagezoomcrop.photoview.IGetImageBounds;
import com.imagezoomcrop.photoview.PhotoView;
import com.imagezoomcrop.photoview.RotationSeekBar;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.FilterActivity;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.FragmentsCorner.CameraCropFragment;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.FragmentsCorner.UpdatedGallery;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.HomeActivity;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.R;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Utilities.StaticBitmapHolder;
import com.soundcloud.android.crop.Crop;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pujan paudel on 11/16/2015.
 */
public class ImageCropFragment extends Fragment {
    public static final String TAG = "ImageCropFragment";
    private static final int ANCHOR_CENTER_DELTA = 10;

    PhotoView mImageView;
    CropOverlayView mCropOverlayView;
    View mMoveResizeText;
    Button mBtnUndoRotation;

    private ContentResolver mContentResolver;

    private final int IMAGE_MAX_SIZE = 1024;
    private final Bitmap.CompressFormat mOutputFormat = Bitmap.CompressFormat.JPEG;

    //Temp file to save cropped image
    private Uri mSaveUri = null;
    private Uri mImageUri = null;



    //File for capturing camera images
    private File mFileTemp;
    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    public static final int REQUEST_CODE_PICK_GALLERY = 0x1;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    public static final int REQUEST_CODE_CROPPED_PICTURE = 0x3;
    public static final String ERROR_MSG = "error_msg";
    public static final String ERROR = "error";


    public static ImageCropFragment reference;
    //activity_image_crop is the fragment to be transacted
    private Bitmap rootbitmap;
    public  ImageCropFragment(){

    }
    @SuppressLint("ValidFragment")
    public ImageCropFragment(Uri source){
        mImageUri=source; //Manipulate here
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        reference=this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_image_crop, container, false);
        mContentResolver = getActivity().getContentResolver();
        mImageView = (PhotoView)view.findViewById(R.id.iv_photo);
        mCropOverlayView = (CropOverlayView)view.findViewById(R.id.crop_overlay);


        mImageView.setImageBoundsListener(new IGetImageBounds() {
            @Override
            public Rect getImageBounds() {
                return mCropOverlayView.getImageBounds();
            }
        });

        //createTempFile(); //Creatng Path of temporayr file
        mSaveUri =Uri.fromFile(new File(getActivity().getCacheDir(), "cropped"));

        //    mImageUri = Utils.getImageUri(mImagePath);
        init();
        return view;
    }


    private void createTempFile() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
        } else {
            mFileTemp = new File(getActivity().getFilesDir(), TEMP_PHOTO_FILE_NAME);
        }
    }
    private void init() {
        System.gc();
        rootbitmap = getBitmap(mImageUri);
        Drawable drawable = new BitmapDrawable(getResources(), rootbitmap);
        float minScale = mImageView.setMinimumScaleToFit(drawable);
        mImageView.setMaximumScale(minScale * 3);
        mImageView.setMediumScale(minScale * 2);
        mImageView.setScale(minScale);
        mImageView.setImageDrawable(drawable);
        //Initialize the MoveResize text
        //RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mMoveResizeText.getLayoutParams();
        //lp.setMargins(0, Math.round(Edge.BOTTOM.getCoordinate()) + 20, 0, 0);
        //mMoveResizeText.setLayoutParams(lp);
    }

    private Bitmap getBitmap(Uri uri) {
        InputStream in = null;
        Bitmap returnedBitmap = null;
        try {
            in = mContentResolver.openInputStream(uri);
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();
            int scale = 1;
            if (o.outHeight > mImageView.getHeight() || o.outWidth > mImageView.getWidth()) {
                scale = (int) Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            in = mContentResolver.openInputStream(uri);
            Bitmap bitmap = BitmapFactory.decodeStream(in, null, o2);
            in.close();

            //First check
            ExifInterface ei = new ExifInterface(uri.getPath());
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    returnedBitmap = rotateImage(bitmap, 90);
                    //Free up the memory
                    bitmap.recycle();
                    bitmap = null;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    returnedBitmap = rotateImage(bitmap, 180);
                    //Free up the memory
                    bitmap.recycle();
                    bitmap = null;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    returnedBitmap = rotateImage(bitmap, 270);
                    //Free up the memory
                    bitmap.recycle();
                    bitmap = null;
                    break;
                default:
                    returnedBitmap = bitmap;
            }
            return returnedBitmap;
        } catch (FileNotFoundException e) {e.printStackTrace();
        } catch (IOException e) {
e.printStackTrace();        }
        return null;
    }
    private Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }


        public void  saveOutput() {
             ProgressDialog progressDialog=new ProgressDialog(UpdatedGallery.reference);
            progressDialog.setMessage("Cropping Image");
            progressDialog.show();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    System.gc();
                   // ProgressDialog progressDialog=new ProgressDialog(UpdatedGallery.reference);
                    //progressDialog.setMessage("Cropping Image");
                    //progressDialog.show();

                    Bitmap croppedImage = mImageView.getCroppedImage();

                    OutputStream outputStream = null;
                    try {
                        outputStream = mContentResolver.openOutputStream(mSaveUri);
                        if (outputStream != null) {
                            croppedImage.compress(mOutputFormat, 100, outputStream);
                            //Do The ProgressDialog Stuff
                            Log.d("Finished", "Compressing");
                            closeSilently(outputStream);
                            StaticBitmapHolder.setCroppedimageUri(mSaveUri);
                            Intent filter=new Intent(UpdatedGallery.reference, FilterActivity.class);
                            //StaticBitmapHolder.setCroppedBitmap(croppedImage);
                             UpdatedGallery.reference.startActivity(filter);
                        }

                    } catch (IOException ex) {
                        ex.printStackTrace();

                    } finally {
                        closeSilently(outputStream);
                    }



                }
            }).start();

    }

    public void closeSilently(Closeable c) {
        if (c == null) return;
        try {
            c.close();
        } catch (Throwable t) {
            // do nothing
        }
    }
@Override
    public void onDestroy(){
    super.onDestroy();
    Log.i("Destroyed", "Some Hope");
    if(rootbitmap!=null){
        rootbitmap.recycle();
        rootbitmap=null;
        System.gc();

    }
}

    @Override
    public void onDetach(){
        super.onDetach();
        Log.i("Detached", "Some Hope");
        if(rootbitmap!=null){
            rootbitmap.recycle();
            rootbitmap=null;
            System.gc();
        }

    }
}
