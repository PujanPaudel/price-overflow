package com.com.fenchtose.touch2focus;


import android.app.ActionBar;
import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.nepalimutu.pujanpaudel.app.priceoverrrflow.R;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.UltimateCamera.BaseFragment;

public class TouchActivity extends BaseFragment {
	
	private PreviewSurfaceView camView;
	private CameraPreview cameraPreview;
	private DrawingView drawingView;
	private Button captureButton;
	private ImageButton switchCamera,switchFlash;
	private int previewWidth = 1280;
	private int previewHeight = 720;
	public static TouchActivity reference;

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
reference=this;

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.activity_touch, container, false);
		camView = (PreviewSurfaceView)view.findViewById(R.id.preview_surface);
		final SurfaceHolder camHolder = camView.getHolder();
		captureButton=(Button)view.findViewById(R.id.button_capture);
		switchCamera=(ImageButton)view.findViewById(R.id.switchCam);
		switchFlash=(ImageButton)view.findViewById(R.id.switchFlash);

		switchCamera.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				cameraPreview.switchCamera();
			}
		});
		switchFlash.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				cameraPreview.switchflash();
			}
		});
		captureButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				cameraPreview.capturePicture();
			}
		});
		cameraPreview = new CameraPreview(previewWidth, previewHeight);
		camHolder.addCallback(cameraPreview);
		camHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

		camView.setListener(cameraPreview);
		//cameraPreview.changeExposureComp(-currentAlphaAngle);
		drawingView = (DrawingView)view.findViewById(R.id.drawing_surface);
		camView.setDrawingView(drawingView);
		return view;

	}


	@Override
	public void onDestroy() {
		super.onDestroy();
		releaseCameraAndPreview();
	}
public void releaseCameraAndPreview(){
	cameraPreview.releaseCamera();
}

	public void cropCancelled(){
		cameraPreview.stopLoading();
	}

	@Override
	public void onPause(){
		Log.d("Touch Activity", "onPause");
		super.onPause();
		//cameraPreview.releaseCamera();
	}
}
