/*
 * Copyright (C) 2014 pengjianbo(pengjianbosoft@gmail.com), Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.cn.finalteam.galleryfinal;

import android.app.Activity;
import android.app.ActivityManager;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cn.finalteam.galleryfinal.adapter.FolderListAdapter;
import com.cn.finalteam.galleryfinal.adapter.PhotoListAdapter;
import com.cn.finalteam.galleryfinal.model.PhotoFolderInfo;
import com.cn.finalteam.galleryfinal.model.PhotoInfo;
import com.cn.finalteam.galleryfinal.utils.PhotoTools;
import com.com.Utilities.ImageCropFragment;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.FragmentsCorner.UpdatedGallery;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.HomeActivity;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.R;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.finalteam.toolsfinal.DeviceUtils;
import cn.finalteam.toolsfinal.StringUtils;

/**
 *
 * Desction:图片选择器
 * Author:pengjianbo
 * Date:15/10/10 下午3:54
 */
public class PhotoChooseActivity extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener{

    private GridView mGvPhotoList;
   // private ListView mLvFolderList;
   // private LinearLayout mLllBottomBar;
    //private LinearLayout mLlFolderPanel;
    //private TextView mTvChooseFolderName;
    private TextView mTvEmptyView;

    //private List<PhotoFolderInfo> mAllPhotoFolderList;
    //private FolderListAdapter mFolderListAdapter;

    private List<PhotoInfo> mCurPhotoList;
    private PhotoListAdapter mPhotoListAdapter;

    private int mLimit;
    private int mPickMode = GalleryHelper.SINGLE_IMAGE;
    private boolean mCrop;
    protected static Map<String, PhotoInfo> mSelectPhotoMap = new HashMap<>();
    protected int mScreenWidth = 720;
    protected int mScreenHeight = 1280;
    protected  String mPhotoTargetFolder;

    public static ImageLoader mImageLoader; //Dont know
    //The Upper needds to be changed as per the width and height at runtime
    //Using Device Utils.java
    public  static PhotoChooseActivity reference;


    //是否需要刷新相册
    private boolean mHasRefreshGallery = false;

    private Handler mHanlder = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if ( msg.what == 1000 ) {
                PhotoInfo photoInfo = (PhotoInfo) msg.obj;
                takeRefreshGallery(photoInfo);
            } else {
                mPhotoListAdapter.notifyDataSetChanged();
                UpdatedGallery.reference.mFolderListAdapter.notifyDataSetChanged();
                if (UpdatedGallery.reference.mAllPhotoFolderList.get(0).getPhotoList() == null ||
                        UpdatedGallery.reference.mAllPhotoFolderList.get(0).getPhotoList().size() == 0) {
                    mTvEmptyView.setText("No picture");
                }

                mGvPhotoList.setEnabled(true);
                UpdatedGallery.reference.mTvChooseFolderName.setEnabled(true);

            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.gf_activity_photo_choose, container, false);
        mGvPhotoList = (GridView)view.findViewById(R.id.gv_photo_list);
        mTvEmptyView = (TextView)view.findViewById(R.id.tv_empty_view);

        mCurPhotoList = new ArrayList<>();
        //These 2 Arguments are not coming from the DeviceUtils.java Of Course !!
        mPhotoListAdapter = new PhotoListAdapter(getActivity(), mCurPhotoList, mSelectPhotoMap, mScreenWidth, mPickMode);
        mGvPhotoList.setAdapter(mPhotoListAdapter);
        mGvPhotoList.setEmptyView(mTvEmptyView);
        setListener();
        refreshSelectCount();
        getPhotos();
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reference=this;
        mLimit =1;
        mPickMode = GalleryHelper.SINGLE_IMAGE;
        mCrop = false; //No Crop Enabled
        mImageLoader =new GalleryImageLoader();
        DisplayMetrics dm = DeviceUtils.getScreenPix(getActivity());
        mScreenWidth = dm.widthPixels;
        mScreenHeight = dm.heightPixels;
       // mPhotoTargetFolder = null;  // This Stuff is Coming From Our Upper Fragment



    }

    private void setListener() {
        //mLllBottomBar.setOnClickListener(this);
        //mLvFolderList.setOnItemClickListener(this);
        mGvPhotoList.setOnItemClickListener(this);
    }

    /**
     * 解决在5.0手机上刷新Gallery问题，从startActivityForResult回到Activity把数据添加到集合中然后理解跳转到下一个页面，
     * adapter的getCount与list.size不一致，所以我这里用了延迟刷新数据
     * @param photoInfo
     */
    protected void takeRefreshGallery(PhotoInfo photoInfo) {
        mCurPhotoList.add(photoInfo);
        mPhotoListAdapter.notifyDataSetChanged();

        //添加到集合中
        List<PhotoInfo> photoInfoList = UpdatedGallery.reference.mAllPhotoFolderList.get(0).getPhotoList();
        if (photoInfoList == null) {
            photoInfoList = new ArrayList<>();
        }
        photoInfoList.add(photoInfo);
        UpdatedGallery.reference.mAllPhotoFolderList.get(0).setPhotoList(photoInfoList);

        if ( UpdatedGallery.reference.mFolderListAdapter.getSelectFolder() != null ) {
            PhotoFolderInfo photoFolderInfo = UpdatedGallery.reference.mFolderListAdapter.getSelectFolder();
            List<PhotoInfo> list = photoFolderInfo.getPhotoList();
            if ( list == null ) {
                list = new ArrayList<>();
            }
            list.add(photoInfo);
            if ( list.size() == 1 ) {
                photoFolderInfo.setCoverPhoto(photoInfo);
            }
            UpdatedGallery.reference.mFolderListAdapter.getSelectFolder().setPhotoList(list);
        } else {
            String folderA = new File(photoInfo.getPhotoPath()).getParent();
            for (int i = 1; i < UpdatedGallery.reference.mAllPhotoFolderList.size(); i++) {
                PhotoFolderInfo folderInfo = UpdatedGallery.reference.mAllPhotoFolderList.get(i);
                String folderB = null;
                if (!StringUtils.isEmpty(photoInfo.getPhotoPath())) {
                    folderB = new File(photoInfo.getPhotoPath()).getParent();
                }
                if (TextUtils.equals(folderA, folderB)) {
                    List<PhotoInfo> list = folderInfo.getPhotoList();
                    if (list == null) {
                        list = new ArrayList<>();
                    }
                    list.add(photoInfo);
                    folderInfo.setPhotoList(list);
                    if ( list.size() == 1 ) {
                        folderInfo.setCoverPhoto(photoInfo);
                    }
                }
            }
        }

        UpdatedGallery.reference.mFolderListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if ( id == R.id.ll_bottom_bar ) {
            if ( UpdatedGallery.reference.mLlFolderPanel.getVisibility() == View.VISIBLE ) {
                UpdatedGallery.reference.mLlFolderPanel.setVisibility(View.GONE);
            } else {
                UpdatedGallery.reference.mLlFolderPanel.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        int parentId = parent.getId();
        if ( parentId == R.id.lv_folder_list ) {
            folderItemClick(position);
        } else {
            photoItemClick(view, position);
        }
    }
    public void folderItemClick(int position) {
        UpdatedGallery.reference.mLlFolderPanel.setVisibility(View.GONE);
        mCurPhotoList.clear();
        PhotoFolderInfo photoFolderInfo = UpdatedGallery.reference.mAllPhotoFolderList.get(position);
        if ( photoFolderInfo.getPhotoList() != null ) {
            mCurPhotoList.addAll(photoFolderInfo.getPhotoList());
        }
        mPhotoListAdapter.notifyDataSetChanged();

        if (position == 0) {
            mPhotoTargetFolder = null;
        } else {
            PhotoInfo photoInfo = photoFolderInfo.getCoverPhoto();
            if (photoInfo != null && !StringUtils.isEmpty(photoInfo.getPhotoPath())) {
                mPhotoTargetFolder = new File(photoInfo.getPhotoPath()).getParent();
            } else {
                mPhotoTargetFolder = null;
            }
        }
        UpdatedGallery.reference.mTvChooseFolderName.setText(photoFolderInfo.getFolderName());
        UpdatedGallery.reference.mFolderListAdapter.setSelectFolder(photoFolderInfo);
        UpdatedGallery.reference.mFolderListAdapter.notifyDataSetChanged();
    }

    private void photoItemClick(View view, int position) {
        PhotoInfo info = mCurPhotoList.get(position);
        if (mPickMode == GalleryHelper.SINGLE_IMAGE) {
            if (mCrop) {
                mHasRefreshGallery = true;
            } else {

                Log.d("Photo Path",info.getPhotoPath());
                UpdatedGallery.reference.Crop(info.getPhotoPath());
            }
            return;
        }
        boolean checked = false;
        if (mSelectPhotoMap.get(info.getPhotoPath()) == null) {
            if (mPickMode == GalleryHelper.MULTIPLE_IMAGE && mSelectPhotoMap.size() == mLimit) {
                Toast.makeText(getActivity().getApplicationContext(),"It has reached the maximum number of selection",Toast.LENGTH_SHORT).show();
                return;
            } else {
                mSelectPhotoMap.put(info.getPhotoPath(), info);
                checked = true;
            }
        } else {
            mSelectPhotoMap.remove(info.getPhotoPath());
            checked = false;
        }
        refreshSelectCount();

        PhotoListAdapter.PhotoViewHolder holder = (PhotoListAdapter.PhotoViewHolder) view.getTag();
        if (holder != null) {
            if (checked) {

                holder.mIvCheck.setBackgroundColor(getColorByTheme(R.attr.colorTheme));
            } else {
                holder.mIvCheck.setBackgroundColor(getResources().getColor(R.color.gf_gray));
            }
        } else {
            mPhotoListAdapter.notifyDataSetChanged();
        }
    }

    public void refreshSelectCount() {
    }

    /**
     * 获取所有图片
     */
    private void getPhotos() {
        mTvEmptyView.setText("请稍后…");
        mGvPhotoList.setEnabled(false);
        UpdatedGallery.reference.mTvChooseFolderName.setEnabled(false);
        new Thread() {
            @Override
            public void run() {
                super.run();

                UpdatedGallery.reference.mAllPhotoFolderList.clear();
                List<PhotoFolderInfo> allFolderList = PhotoTools.getAllPhotoFolder(getActivity());
                UpdatedGallery.reference.mAllPhotoFolderList.addAll(allFolderList);

                mCurPhotoList.clear();
                if ( allFolderList.size() > 0 ) {
                    if ( allFolderList.get(0).getPhotoList() != null ) {
                        mCurPhotoList.addAll(allFolderList.get(0).getPhotoList());
                    }
                }

                mHanlder.sendEmptyMessage(1);
            }
        }.start();
    }

    @Override
    public void onResume() {
        super.onResume();
        //if ( GalleryHelper.mImageLoader == null ) {
        //    Toast.makeText(getApplicationContext(),"Select a picture fails",Toast.LENGTH_LONG).show();
        //    finish();
        //}

        if ( mHasRefreshGallery ) {
            mHasRefreshGallery = false;
            getPhotos();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPhotoTargetFolder = null;
        mSelectPhotoMap.clear();
        mLimit = 1;
        mPickMode = GalleryHelper.SINGLE_IMAGE;
        mCrop = false;
        System.gc();
    }
    public StateListDrawable getTitleStateListDrawable() {
        StateListDrawable bg = new StateListDrawable();
        bg.addState(new int[]{android.R.attr.state_pressed}, new ColorDrawable(getColorByTheme(R.attr.colorThemeDark)));
        bg.addState(new int[]{}, new ColorDrawable(getColorByTheme(R.attr.colorTheme)));
        return bg;
    }
    public int getColorByTheme(int attr) {
        TypedValue typedValue = new TypedValue();
        getActivity().getTheme().resolveAttribute(attr, typedValue, true);
        int colorTheme = typedValue.data;
        return colorTheme;
    }


}
