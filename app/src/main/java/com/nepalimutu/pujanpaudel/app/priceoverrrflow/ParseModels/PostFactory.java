package com.nepalimutu.pujanpaudel.app.priceoverrrflow.ParseModels;

import android.app.ProgressDialog;
import android.graphics.Picture;

import com.nepalimutu.pujanpaudel.app.priceoverrrflow.FragmentsCorner.PublishFragment;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONArray;

/**
 * Created by pujan paudel on 11/19/2015.
 */
public class PostFactory {
    /**
     * @Params of which we are going to derive the model
     * Poster(pointer)
     * PosterName(String)
     * Picture(File)
     * Caption(String)
     * HashTags(String Array)
     * Favourites(Number)
     * Favourites Array(Array)
     *TargetAudience (Array)
     */
    private ParseUser poster;
    private String postername;
    private ParseFile photofile;
    private String caption;
    private JSONArray hashtags;
    private int favouritescount;
    private JSONArray favouritesarray;
    private JSONArray targetaudience;
    private  byte[]imagedata;

    public PostFactory(ParseUser poster, String postername, ParseFile photofile, String caption, JSONArray hashtags, int favouritescount, JSONArray favouritesarray, JSONArray targetaudience,byte[] imagedata) {
        this.poster = poster;
        this.postername = postername;
        this.photofile = photofile;
        this.caption = caption;
        this.hashtags = hashtags;
        this.favouritescount = favouritescount;
        this.favouritesarray = favouritesarray;
        this.targetaudience = targetaudience;
        this.imagedata=imagedata;
    }


    public void Parcel(){
        final ProgressDialog saving=new ProgressDialog(PublishFragment.reference);
        saving.setMessage("Doing the post ");
        saving.show();
        ParsePost newPost=new ParsePost();
        newPost.setPoster(poster);
        newPost.setPosterName(postername);
        newPost.setPicture(photofile); //--> A little Workaround perhaps
        newPost.setCaption(caption);
        newPost.setHashTags(hashtags);
        newPost.setFavourites(favouritescount);
        newPost.setFavouritesArray(favouritesarray);
        newPost.setTargetAudience(targetaudience);
       // newPost.setPictureData(imagedata);
        ParseACL dataACl=new ParseACL();
        dataACl.setPublicReadAccess(true);// Maybe we have to set this to false
        dataACl.setPublicWriteAccess(true); //Set this to false
        newPost.setACL(dataACl);
        newPost.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e==null){
                    /**
                     * Picture posted without any Error
                     */
                saving.dismiss();
                }else{
                    /**
                     * Picture posted with error
                     * Do Some Reprocessig Stuffs though
                     */
                    e.printStackTrace();
                }
            }
        });
    }
}
