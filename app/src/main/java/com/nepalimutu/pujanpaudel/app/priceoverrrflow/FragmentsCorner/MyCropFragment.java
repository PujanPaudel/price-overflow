package com.nepalimutu.pujanpaudel.app.priceoverrrflow.FragmentsCorner;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.opengl.GLES10;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.nepalimutu.pujanpaudel.app.priceoverrrflow.FilterActivity;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.R;
import com.soundcloud.android.crop.Crop;
import com.soundcloud.android.crop.CropImageView;
import com.soundcloud.android.crop.CropUtil;
import com.soundcloud.android.crop.GalleryCropImageView;
import com.soundcloud.android.crop.HighlightView;
import com.soundcloud.android.crop.ImageViewTouchBase;
import com.soundcloud.android.crop.RotateBitmap;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by pujan paudel on 11/9/2015.
 */
public class MyCropFragment extends Fragment {
    private static final int SIZE_DEFAULT = 2048;
    private static final int SIZE_LIMIT = 4096;

    private final Handler handler = new Handler();

    private int aspectX;
    private int aspectY;
    int recycleCount=0;
    // Output image
    private int maxX=800;
    private int maxY=800;
    private int exifRotation;

    private Uri sourceUri;
    private Uri saveUri;

    private boolean isSaving;

    private int sampleSize;
    private RotateBitmap rotateBitmap;
    private GalleryCropImageView imageView;
    private HighlightView cropView;

    //The Buttons for the Done and Cancel
    FrameLayout save,cancel;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        saveUri=Uri.fromFile(new File(getActivity().getCacheDir(), "cropped"));
    }

    public MyCropFragment(){

    }
    @SuppressLint("ValidFragment")
    public MyCropFragment(Uri targetUri){
        sourceUri=targetUri;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mycropfragment, container, false);
        imageView=(GalleryCropImageView)view.findViewById(R.id.preview);
        save=(FrameLayout)view.findViewById(R.id.btn_done);
        cancel=(FrameLayout)view.findViewById(R.id.btn_cancel);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PhotoGalleryFragment.reference.stopLoading();
                onSaveClicked();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel.setVisibility(View.GONE);
                save.setVisibility(View.GONE);
                //Reset the Crop View
            }
        });
        setCropView();
        loadInput(sourceUri);
        startCrop();
        return view;
    }



    public void setCropView(){
        //Inflate  The Xml View First
        //Referncce of the ImageView From XML
        imageView.context=getActivity();
        imageView.setRecycler(new ImageViewTouchBase.Recycler() {
            @Override
            public void recycle(Bitmap b) {
                b.recycle();
                System.gc();
            }
        });

    }


    private void loadInput(Uri sourceUri){
        //assign to Rotate Bitmap
        if (sourceUri != null) {
            exifRotation = CropUtil.getExifRotation(CropUtil.getFromMediaUri(getActivity(), getActivity().getContentResolver(), sourceUri));
            InputStream is = null;
            try {
                sampleSize = calculateBitmapSampleSize(sourceUri);
                Log.i("Sample Size",String.valueOf(sampleSize));
                is = getActivity().getContentResolver().openInputStream(sourceUri);
                BitmapFactory.Options option = new BitmapFactory.Options();
                option.inSampleSize =sampleSize;
               // rotateBitmap = new RotateBitmap(BitmapFactory.decodeStream(is, null, option), exifRotation);
                rotateBitmap=new RotateBitmap(BitmapFactory.decodeStream(is,null,option),exifRotation);
            } catch (IOException e) {
                // com.soundcloud.android.crop.Log.e("Error reading image: " + e.getMessage(), e);
                setResultException(e);
            } catch (OutOfMemoryError e) {
                //com.soundcloud.android.crop.Log.e("OOM reading image: " + e.getMessage(), e);
                setResultException(e);
            } finally {
                CropUtil.closeSilently(is);
            }
        }
    }
    private int calculateBitmapSampleSize(Uri bitmapUri) throws IOException {
        InputStream is = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            is = getActivity().getContentResolver().openInputStream(bitmapUri);
            BitmapFactory.decodeStream(is, null, options); // Just get image size
        } finally {
            CropUtil.closeSilently(is);
        }

        int maxSize = getMaxImageSize();
        int sampleSize = 1;
        while (options.outHeight / sampleSize > maxSize || options.outWidth / sampleSize > maxSize) {
            sampleSize = sampleSize << 1;
        }
        return sampleSize;
    }

    private int getMaxImageSize() {
        int textureLimit = getMaxTextureSize();
        if (textureLimit == 0) {
            return SIZE_DEFAULT;
        } else {
            return Math.min(textureLimit, SIZE_LIMIT);
        }
    }

    private int getMaxTextureSize() {
        // The OpenGL texture size is the maximum size that can be drawn in an ImageView
        int[] maxSize = new int[1];
        GLES10.glGetIntegerv(GLES10.GL_MAX_TEXTURE_SIZE, maxSize, 0);
        return maxSize[0];
    }




    private class Cropper {

        private void makeDefault() {
            if (rotateBitmap == null) {
                return;
            }
            HighlightView hv = new HighlightView(imageView);
            final int width = rotateBitmap.getWidth();
            final int height = rotateBitmap.getHeight();

            Rect imageRect = new Rect(0, 0, width, height);

            // Make the default size about 4/5 of the width or height
            int cropWidth = Math.min(width, height) *1;  //This PArameter changes the Width Height
            @SuppressWarnings("SuspiciousNameCombination")
            int cropHeight = cropWidth;

            if (aspectX != 0 && aspectY != 0) {
                if (aspectX > aspectY) {
                    cropHeight = cropWidth * aspectY / aspectX;
                } else {
                    cropWidth = cropHeight * aspectX / aspectY;
                }
            }

            int x = (width - cropWidth) / 2;
            int y = (height - cropHeight) / 2;

            RectF cropRect = new RectF(x, y, x + cropWidth, y + cropHeight);
            hv.setup(imageView.getUnrotatedMatrix(), imageRect, cropRect, aspectX != 0 && aspectY != 0);
            imageView.add(hv);
        }

        public void crop() {
            handler.post(new Runnable() {
                public void run() {
                    makeDefault();
                    imageView.invalidate();
                    if (imageView.highlightViews.size() == 1) {
                        cropView = imageView.highlightViews.get(0);
                        cropView.setFocus(true);
                    }
                }
            });
        }
        private void saveImage(Bitmap croppedImage) {
            if (croppedImage != null) {
                final Bitmap b = croppedImage;
                //Do Some Showing of Crop Option
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        saveOutput(b);
                    }
                });


            } else {
                // finish();
            }
        }


        private Bitmap decodeRegionCrop(Rect rect, int outWidth, int outHeight) {
            // Release memory now
            clearImageView();

            InputStream is = null;
            Bitmap croppedImage = null;
            try {
                is = getActivity().getContentResolver().openInputStream(sourceUri);
                BitmapRegionDecoder decoder = BitmapRegionDecoder.newInstance(is, false);
                final int width = decoder.getWidth();
                final int height = decoder.getHeight();

                if (exifRotation != 0) {
                    // Adjust crop area to account for image rotation
                    Matrix matrix = new Matrix();
                    matrix.setRotate(-exifRotation);

                    RectF adjusted = new RectF();
                    matrix.mapRect(adjusted, new RectF(rect));

                    // Adjust to account for origin at 0,0
                    adjusted.offset(adjusted.left < 0 ? width : 0, adjusted.top < 0 ? height : 0);
                    rect = new Rect((int) adjusted.left, (int) adjusted.top, (int) adjusted.right, (int) adjusted.bottom);
                }

                try {
                    croppedImage = decoder.decodeRegion(rect, new BitmapFactory.Options());
                    if (rect.width() > outWidth || rect.height() > outHeight) {
                        Matrix matrix = new Matrix();
                        matrix.postScale((float) outWidth / rect.width(), (float) outHeight / rect.height());
                        croppedImage = Bitmap.createBitmap(croppedImage, 0, 0, croppedImage.getWidth(), croppedImage.getHeight(), matrix, true);
                    }
                } catch (IllegalArgumentException e) {
                    // Rethrow with some extra information
                    throw new IllegalArgumentException("Rectangle " + rect + " is outside of the image ("
                            + width + "," + height + "," + exifRotation + ")", e);
                }

            } catch (IOException e) {
                // com.soundcloud.android.crop.Log.e("Error cropping image: " + e.getMessage(), e);
                setResultException(e);
            } catch (OutOfMemoryError e) {
                //com.soundcloud.android.crop.Log.e("OOM cropping image: " + e.getMessage(), e);
                setResultException(e);
            } finally {
                CropUtil.closeSilently(is);
            }
            return croppedImage;
        }
    }

    private void clearImageView() {
        imageView.clear();
        if (rotateBitmap != null) {
            rotateBitmap.recycle();
        }
        System.gc();
    }






    private void processtoFilter(Uri uri) {
        //getActivity().setResult(RESULT_OK, new Intent().putExtra(MediaStore.EXTRA_OUTPUT, uri));
        //Do the Task Here :
Toast.makeText(getActivity(),"Redirecting to fileter",Toast.LENGTH_LONG).show();
        Intent filter=new Intent(BaseImagesContainer.reference, FilterActivity.class);
        filter.putExtra("Bitmap", uri.getPath());
        getActivity().startActivity(filter);
    }

    private void setResultException(Throwable throwable) {
        //    setResult(Crop.RESULT_ERROR, new Intent().putExtra(Crop.Extra.ERROR, throwable));
    }

    private void startCrop() {
        imageView.setImageRotateBitmapResetBase(rotateBitmap, true);
        if (imageView.getScale() == 1F) {
            imageView.center();
        }
        new Cropper().crop();
    }
    private void onSaveClicked() {
        if (cropView == null || isSaving) {
            Toast.makeText(getActivity(),"Technical Issues in Cropping",Toast.LENGTH_LONG).show();
            return;
        }else{
            Toast.makeText(getActivity(),"Cropping the Image ",Toast.LENGTH_LONG).show();

        }
        isSaving = true;

        Bitmap croppedImage;
        Rect r = cropView.getScaledCropRect(sampleSize);
        int width = r.width();
        int height = r.height();

        int outWidth = width;
        int outHeight = height;
        if (maxX > 0 && maxY > 0 && (width > maxX || height > maxY)) {
            float ratio = (float) width / (float) height;
            if ((float) maxX / (float) maxY > ratio) {
                outHeight = maxY;
                outWidth = (int) ((float) maxY * ratio + .5f);
            } else {
                outWidth = maxX;
                outHeight = (int) ((float) maxX / ratio + .5f);
            }
        }

        try {
            croppedImage = decodeRegionCrop(r, outWidth, outHeight);
        } catch (IllegalArgumentException e) {
            setResultException(e);
            //finish();
            e.printStackTrace();
            return;
        }

        if (croppedImage != null) {
            imageView.setImageRotateBitmapResetBase(new RotateBitmap(croppedImage, exifRotation), true);
            imageView.center();
            imageView.highlightViews.clear();
        }
        saveImage(croppedImage);
    }

    private void saveImage(Bitmap croppedImage) {
        Log.i("ByPass", "SaveImage");
        if (croppedImage != null) {
            final Bitmap b = croppedImage;
            saveOutput(b);
        }
    }


    private Bitmap decodeRegionCrop(Rect rect, int outWidth, int outHeight) {
        // Release memory now
        Log.d("ByPass","DecodeRegionCrop");
        clearImageView();
        InputStream is = null;
        Bitmap croppedImage = null;
        try {
            is = getActivity().getContentResolver().openInputStream(sourceUri);
            BitmapRegionDecoder decoder = BitmapRegionDecoder.newInstance(is, false);
            final int width = decoder.getWidth();
            final int height = decoder.getHeight();

            if (exifRotation != 0) {
                // Adjust crop area to account for image rotation
                Matrix matrix = new Matrix();
                matrix.setRotate(-exifRotation);

                RectF adjusted = new RectF();
                matrix.mapRect(adjusted, new RectF(rect));

                // Adjust to account for origin at 0,0
                adjusted.offset(adjusted.left < 0 ? width : 0, adjusted.top < 0 ? height : 0);
                rect = new Rect((int) adjusted.left, (int) adjusted.top, (int) adjusted.right, (int) adjusted.bottom);
            }

            try {
                croppedImage = decoder.decodeRegion(rect, new BitmapFactory.Options());
                if (rect.width() > outWidth || rect.height() > outHeight) {
                    Matrix matrix = new Matrix();
                    matrix.postScale((float) outWidth / rect.width(), (float) outHeight / rect.height());
                    croppedImage = Bitmap.createBitmap(croppedImage, 0, 0, croppedImage.getWidth(), croppedImage.getHeight(), matrix, true);
                }
            } catch (IllegalArgumentException e) {
                // Rethrow with some extra information
                throw new IllegalArgumentException("Rectangle " + rect + " is outside of the image ("
                        + width + "," + height + "," + exifRotation + ")", e);
            }

        } catch (IOException e) {
            //Log.e("Error cropping image: " + e.getMessage(), e);
            setResultException(e);
        } catch (OutOfMemoryError e) {
            //Log.e("OOM cropping image: " + e.getMessage(), e);
            setResultException(e);
        } finally {
            CropUtil.closeSilently(is);
        }
        return croppedImage;
    }


    private void saveOutput(Bitmap croppedImage) {
        Log.i("ByPass","SaveOutput");
        Log.d("SaveUri",saveUri.getPath());
        if (saveUri != null) {
            OutputStream outputStream = null;
            try {
                outputStream = getActivity().getContentResolver().openOutputStream(saveUri);
                if (outputStream != null) {
                    croppedImage.compress(Bitmap.CompressFormat.PNG,100, outputStream);
                }
            } catch (IOException e) {
                setResultException(e);
                e.printStackTrace();
                //Log.e("Cannot open file: " + saveUri, e);
            } finally {
                CropUtil.closeSilently(outputStream);
            }

            CropUtil.copyExifRotation(
                    CropUtil.getFromMediaUri(getActivity(), getActivity().getContentResolver(), sourceUri),
                    CropUtil.getFromMediaUri(getActivity(), getActivity().getContentResolver(), saveUri)
            );
Log.d("Processing","To Filter");
            processtoFilter(saveUri);
        }

        final Bitmap b = croppedImage;
        handler.post(new Runnable() {
            public void run() {
                imageView.clear();
                b.recycle();
            }
        });

        //finish();
    }

    public boolean isSaving() {
        return isSaving;
    }


}
