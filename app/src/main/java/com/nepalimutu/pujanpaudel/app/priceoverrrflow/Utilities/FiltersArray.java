package com.nepalimutu.pujanpaudel.app.priceoverrrflow.Utilities;


import com.nepalimutu.pujanpaudel.app.priceoverrrflow.R;

/**
 * Created by pujan paudel on 11/5/2015.
 */
public class FiltersArray {

    public static String[]availableFilters=new String[]{
            "Normal",
            "Amaro",
            "Nashille",
            "Hudson",
            "Rise",
            "Lomo",
            "Kelvin",
            "EarlyBird",
            "Walden",
            "XproII",
            "Inkwell"
    };

        public static int[]previewFilters=new int[]{
                R.drawable.filter_normal,
               R.drawable.filter_aicao,
                R.drawable.filter_chenjiang,
                R.drawable.filter_heibai,
                R.drawable.filter_jianghuang,
                R.drawable.filter_liuli,
                R.drawable.filter_natie,
                R.drawable.filter_poxiao,
                R.drawable.filter_qingse,
                R.drawable.filter_shentou,
                R.drawable.filter_xpro
        };
}
