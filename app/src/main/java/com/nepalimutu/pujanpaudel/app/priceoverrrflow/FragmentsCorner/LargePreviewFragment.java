package com.nepalimutu.pujanpaudel.app.priceoverrrflow.FragmentsCorner;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nepalimutu.pujanpaudel.app.priceoverrrflow.R;

/**
 * Created by pujan paudel on 11/20/2015.
 */
public class LargePreviewFragment extends Fragment {

    private Bitmap largeBitmap;
    private String caption;

    public LargePreviewFragment(){
    }
    @SuppressLint("ValidFragment")
    public LargePreviewFragment(Bitmap bmp,String cap){
        largeBitmap=bmp;
        caption=cap;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fullscreen_preview, container, false);

        ImageView fullpreview=(ImageView)view.findViewById(R.id.ivFeedCenter);
        TextView captionview=(TextView)view.findViewById(R.id.caption);
        fullpreview.setImageBitmap(largeBitmap);
        captionview.setText(caption);
    return view;

    }
}
