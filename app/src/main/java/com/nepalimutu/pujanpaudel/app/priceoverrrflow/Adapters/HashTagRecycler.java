package com.nepalimutu.pujanpaudel.app.priceoverrrflow.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nepalimutu.pujanpaudel.app.priceoverrrflow.R;

import java.util.List;

/**
 * Created by pujan paudel on 11/20/2015.
 */
public class HashTagRecycler extends RecyclerView.Adapter<HashTagRecycler.HashTagViewHolder>{

    private List<String> hashtags;
    private Context ctx;

    public HashTagRecycler(List<String>hashes){
        this.hashtags=hashes;
    }
    @Override
    public HashTagViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ctx=parent.getContext();
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.hashtag_row, parent, false);
        return new HashTagViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HashTagViewHolder holder, int position) {

        holder.hashtag.setText("#"+hashtags.get(position));
        holder.hashtag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Do Our Searching for Tags Stuff here !!
            }
        });
    }

    @Override
    public int getItemCount() {
        return hashtags.size();
    }

    public static class HashTagViewHolder extends RecyclerView.ViewHolder{
        private TextView hashtag;

        public HashTagViewHolder(View v) {
            super(v);
            hashtag=(TextView)v.findViewById(R.id.hashtag);
        }

    }
}
