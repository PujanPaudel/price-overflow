package com.nepalimutu.pujanpaudel.app.priceoverrrflow.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Models.Feed;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Models.LoadListener;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.ParseModels.ParsePost;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.R;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Utilities.CriticalValuesHolder;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.assist.ViewScaleType;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.nostra13.universalimageloader.core.imageaware.NonViewAware;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.squareup.picasso.Picasso;
import com.todddavies.components.progressbar.ProgressWheel;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pujan paudel on 11/9/2015.
 */
public class PhotoFeedAdapter extends RecyclerView.Adapter<PhotoFeedAdapter.PhotoFeedViewHolder> {
   private List<ParsePost> mfeedList;
    private List<LoadListener>mloaders;
    private Context ctx;
    private int mLastPosition=0;
    private PhotoFeedViewHolder currentholder;
    private ParsePost currentpost;
    DisplayImageOptions options = new DisplayImageOptions.Builder()
            .cacheInMemory(false)
            .cacheOnDisk(true)
            .bitmapConfig(Bitmap.Config.RGB_565)
            .build();


    public PhotoFeedAdapter(List<ParsePost>feedList,List<LoadListener>mloads){
    mfeedList=feedList;
        mloaders=mloads;
}


    @Override
    public PhotoFeedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ctx=parent.getContext();
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_feed, parent, false);
        return new PhotoFeedViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final PhotoFeedViewHolder holder, int position) {
        currentholder=holder;
        float initialTranslation=(mLastPosition<=position?1500f:-150f);

        final ParsePost currentfeed=mfeedList.get(position);
        currentpost=currentfeed;
        holder.currentview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //What To Do when the Feed View is Selected
            }
        });

        LinearLayoutManager llm = new LinearLayoutManager(ctx);
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);

        holder.hashtags.setLayoutManager(llm);
        holder.hashtags.hasFixedSize();
        holder.hashtags.setAdapter(new HashTagRecycler(parseHashTags())); // ->> Data needs to be Parsed from Server

        holder.mycaption.setText(currentfeed.getCaption());
        holder.pic_poster.setText(currentfeed.getPosterName());
        holder.pic_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Liked the Photo", Snackbar.LENGTH_LONG).show();
            }
        });

        holder.pic_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Commented the Photo", Snackbar.LENGTH_LONG).show();
            }
        });
        holder.pic_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "More Abou the Photo ", Snackbar.LENGTH_LONG).show();

            }
        });
        LoadListener currentloader=mloaders.get(position);
        if(currentloader.isComplete()){
            holder.pictureview.setImageBitmap(currentloader.getFinalbmp());
            holder.loading.setVisibility(View.GONE);
        }else{
            holder.loading.setProgress(currentloader.getProgressvalue());
        }

        //Set The Date Here Then



        int flags = DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME|  DateUtils.FORMAT_NO_YEAR;
        long millisecond =currentfeed.getCreatedAt().getTime();
        String monthAndDayText = DateUtils.formatDateTime(ctx, millisecond, flags);

        holder.pic_time.setText(monthAndDayText);
        holder.pic_like_counter.setText(String.valueOf(currentfeed.getFavourites()) + "Likes ");
        holder.itemView.setTranslationY(initialTranslation);
        holder.itemView.animate().setInterpolator(new DecelerateInterpolator(1.0f)).translationY(0f).setDuration(900l).setListener(null);
        mLastPosition=position;
    }

    @Override
    public int getItemCount() {
        return mfeedList.size();
    }

    public class PhotoFeedViewHolder extends RecyclerView.ViewHolder{
        protected CardView currentview;
        protected TextView pic_time;
        protected TextView pic_poster;
        protected ImageView myphoto;
        protected TextView mycaption;
        protected ImageButton pic_like;
        protected ImageButton pic_comment;
        protected ImageButton pic_more;
        protected TextView pic_like_counter;
        protected ProgressWheel loading;
        protected ImageView pictureview;
        protected RecyclerView hashtags;
        public PhotoFeedViewHolder(View v){
            super(v);
            currentview=(CardView)v.findViewById(R.id.card_view);
            pic_time=(TextView)v.findViewById(R.id.iv_time);
            pic_poster=(TextView)v.findViewById(R.id.iv_username);
            myphoto=(ImageView)v.findViewById(R.id.ivFeedCenter);
            mycaption=(TextView)v.findViewById(R.id.caption);
            pic_like=(ImageButton)v.findViewById(R.id.btnLike);
            pic_comment=(ImageButton)v.findViewById(R.id.btnComments);
            pic_more=(ImageButton)v.findViewById(R.id.btnMore);
            pic_like_counter=(TextView)v.findViewById(R.id.like_counter);
            loading=(ProgressWheel)v.findViewById(R.id.pw_spinner);
            pictureview=(ImageView)v.findViewById(R.id.ivFeedCenter);
            hashtags=(RecyclerView)v.findViewById(R.id.hashes);
        }

    }

    public void beginLoading(){
        Log.d("My Size",String.valueOf(mloaders.size()));
        for(final LoadListener listener:mloaders){
            //Get the Fixed Width and Height of our Images
            ImageAware aware=new NonViewAware(new ImageSize(CriticalValuesHolder.getCroppedimagewidth(),CriticalValuesHolder.getCroppedimageheight()),ViewScaleType.CROP);
            ImageLoader.getInstance().displayImage(listener.getMyurl(), aware, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    if(currentpost.getPicture().getUrl().equals(listener.getMyurl())){  //Nearly a Fatal Mistake
                        currentholder.loading.setVisibility(View.GONE);
                        currentholder.pictureview.setImageBitmap(loadedImage);
                    }
                    listener.setComplete(true);
                    listener.setFinalbmp(loadedImage);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {
                        //Failed Bitmap to be shown here
                }
            }, new ImageLoadingProgressListener() {
                @Override
                public void onProgressUpdate(String imageUri, View view, int current, int total) {
                    //Doing the same for progress also as done in Laoding Complete
                    float per=(float)current/total;
                    listener.setProgressvalue(Math.round(per*360));
                    if(currentpost.getPicture().getUrl().equals(listener.getMyurl())){  //Nearly a Fatal Mistake
                        currentholder.loading.setProgress(Math.round(per*360));
                    }
                }
            });

        }
    }

    private List<String>parseHashTags(){
        //Just a temporary data assigning function
        List<String>temp=new ArrayList<>();
        for(int i=0;i<6;i++){
            temp.add("HashTag"+String.valueOf(i));
        }
        return  temp;
    }
}
