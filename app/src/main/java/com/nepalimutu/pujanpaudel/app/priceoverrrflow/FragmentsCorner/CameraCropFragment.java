package com.nepalimutu.pujanpaudel.app.priceoverrrflow.FragmentsCorner;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import com.imagezoomcrop.cropoverlay.CropOverlayView;
import com.imagezoomcrop.photoview.IGetImageBounds;
import com.imagezoomcrop.photoview.PhotoView;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.FilterActivity;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.R;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Utilities.StaticBitmapHolder;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by pujan paudel on 11/18/2015.
 */
public class CameraCropFragment extends AppCompatActivity {
    public static final String TAG = "ImageCropFragment";
    private static final int ANCHOR_CENTER_DELTA = 10;

    PhotoView mImageView;
    CropOverlayView mCropOverlayView;
    View mMoveResizeText;
    Button mBtnUndoRotation;

    private ContentResolver mContentResolver;

    private final int IMAGE_MAX_SIZE = 1024;
    private final Bitmap.CompressFormat mOutputFormat = Bitmap.CompressFormat.JPEG;

    //Temp file to save cropped image
    private Uri mImageUri = null;
    private Uri mSaveUri = null;


    //File for capturing camera images
    private File mFileTemp;
    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";
    public static final int REQUEST_CODE_PICK_GALLERY = 0x1;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    public static final int REQUEST_CODE_CROPPED_PICTURE = 0x3;
    public static final String ERROR_MSG = "error_msg";
    public static final String ERROR = "error";

    private FrameLayout done, cancel;


    public static CameraCropFragment reference;
    //activity_image_crop is the fragment to be transacted
    private Bitmap rootbitmap;

    public CameraCropFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camera_image_crop);
        reference = this;
        mContentResolver = getContentResolver();
        done = (FrameLayout) findViewById(R.id.btn_done);
        cancel = (FrameLayout) findViewById(R.id.btn_cancel);
        mImageView = (PhotoView) findViewById(R.id.iv_photo);
        mCropOverlayView = (CropOverlayView) findViewById(R.id.crop_overlay);
        mImageUri = StaticBitmapHolder.getCameraimageUri();
        mImageView.setImageBoundsListener(new IGetImageBounds() {
            @Override
            public Rect getImageBounds() {
                return mCropOverlayView.getImageBounds();
            }

        });

        mSaveUri = Uri.fromFile(new File(getCacheDir(), "cropped"));

        //createTempFile(); //Creatng Path of temporayr file
        init();
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveOutput();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }


    private void init() {
        rootbitmap = getBitmap(mImageUri);
        Drawable drawable = new BitmapDrawable(getResources(), rootbitmap);
        float minScale = mImageView.setMinimumScaleToFit(drawable);
        mImageView.setMaximumScale(minScale * 3);
        mImageView.setMediumScale(minScale * 2);
        mImageView.setScale(minScale);
        rootbitmap = null;
        System.gc();
        mImageView.setImageDrawable(drawable);
        //Initialize the MoveResize text
        //RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mMoveResizeText.getLayoutParams();
        //lp.setMargins(0, Math.round(Edge.BOTTOM.getCoordinate()) + 20, 0, 0);
        //mMoveResizeText.setLayoutParams(lp);
    }

    private Bitmap getBitmap(Uri uri) {
        InputStream in = null;
        Bitmap returnedBitmap = null;
        try {
            in = mContentResolver.openInputStream(uri);
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();
            int scale = 1;
            if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
                scale = (int) Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            in = mContentResolver.openInputStream(uri);
            Bitmap bitmap = BitmapFactory.decodeStream(in, null, o2);
            in.close();

            //First check
            ExifInterface ei = new ExifInterface(uri.getPath());
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    returnedBitmap = rotateImage(bitmap, 90);
                    //Free up the memory
                    bitmap.recycle();
                    bitmap = null;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    returnedBitmap = rotateImage(bitmap, 180);
                    //Free up the memory
                    bitmap.recycle();
                    bitmap = null;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    returnedBitmap = rotateImage(bitmap, 270);
                    //Free up the memory
                    bitmap.recycle();
                    bitmap = null;
                    break;
                default:
                    returnedBitmap = bitmap;
            }
            return returnedBitmap;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }


    public void saveOutput() {
        ProgressDialog progressDialog = new ProgressDialog(CameraCropFragment.this);
        progressDialog.setMessage("Cropping Image");
        progressDialog.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.gc();
                Bitmap croppedImage = mImageView.getCroppedImage();
                OutputStream outputStream = null;
                try {
                    outputStream = mContentResolver.openOutputStream(mSaveUri);
                    if (outputStream != null) {
                        croppedImage.compress(mOutputFormat, 100, outputStream);
                        //Do The ProgressDialog Stuff
                        Log.d("Finished", "Compressing");
                        closeSilently(outputStream);
                        StaticBitmapHolder.setCroppedimageUri(mSaveUri);
                        Intent filter = new Intent(CameraCropFragment.this, FilterActivity.class);
                        //StaticBitmapHolder.setCroppedBitmap(croppedImage);
                        startActivity(filter);
                    }

                } catch (IOException ex) {
                    ex.printStackTrace();

                } finally {
                }


            }
        }).start();

    }



    public void closeSilently(Closeable c) {
        if (c == null) return;
        try {
            c.close();
        } catch (Throwable t) {
            // do nothing
        }
    }



    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.i("Destroyed", "Some Hope");
        if(rootbitmap!=null){
            rootbitmap.recycle();
            rootbitmap=null;
            System.gc();

        }
    }

}
