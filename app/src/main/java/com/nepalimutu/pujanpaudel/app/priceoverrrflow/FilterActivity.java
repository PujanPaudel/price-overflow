package com.nepalimutu.pujanpaudel.app.priceoverrrflow;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.opengl.GLES10;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Adapters.FiltersAdapter;

import com.nepalimutu.pujanpaudel.app.priceoverrrflow.FragmentsCorner.PublishFragment;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Interfaces.FilterSelectedCallback;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Models.Filter;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Utilities.FiltersArray;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Utilities.StaticBitmapHolder;
import com.soundcloud.android.crop.CropUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageView;

import me.isming.tools.cvfilter.library.*;
/**
 * Created by pujan paudel on 11/5/2015.
 */
public class FilterActivity extends AppCompatActivity implements FilterSelectedCallback,Handler.Callback {
    private RecyclerView recList;
    private FiltersAdapter filtersAdapter;
    private ImageView filterpreview;
    private Bitmap orginalBitmap;
    private ImageData mImageData;
    public static  FilterActivity reference;
    private ICVFilter[]myfilters;
    private Bitmap scaledBitmap;
    private static final int SIZE_DEFAULT = 2048;
    private static final int SIZE_LIMIT = 4096;
    private Bitmap mResultBitmap;
    private final int IMAGE_MAX_SIZE = 1024;
    private ProgressDialog loading;
    //Custom layout so Cacnel and Ok are Not Buttons but FrameLayout
    private FrameLayout done,cancel;

    private Uri mSaveUri = null;
    //The mSaveUri is the final URI of the image that is going to  be saved
    //Like Piksaar -Images
    @Override
    public void onCreate(Bundle savedInstanceState){
        loading=new ProgressDialog(FilterActivity.this);
        loading.setMessage("Loading Filters");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filterfragment);
        filterpreview=(ImageView)findViewById(R.id.image);

        //loading.show();
        done=(FrameLayout)findViewById(R.id.btn_done);
        cancel=(FrameLayout)findViewById(R.id.btn_cancel);
        orginalBitmap= returnSampledBitmap();
        mSaveUri =Uri.fromFile(new File(getCacheDir(), "cropped"));

        //MayBe Doing the DownSAMPLING AGAIN :P
        scaledBitmap=Bitmap.createScaledBitmap(returnSamllestBitmap(),100,98,true);
        mResultBitmap=orginalBitmap;
        mImageData = new ImageData(scaledBitmap);
        ExifInterface ei=null;
        reference=this;
        myfilters=FilterFactory.createFilters(this);
        recList=(RecyclerView)findViewById(R.id.recyclerfilter);
        filterpreview.setImageBitmap(orginalBitmap);

        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        recList.setLayoutManager(llm);
        recList.setHasFixedSize(true);
        filtersAdapter=new FiltersAdapter(returnList());
        recList.setAdapter(filtersAdapter);

        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Additionalcompute1();
                    }
                });
            }
        }).start();


        //
        //computeFilter();
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //StaticBitmapHolder.setFinalBitmap(mResultBitmap); // --> We Dont Need this now or we ?
                OutputStream outputStream = null;
                try{
                    outputStream = getContentResolver().openOutputStream(mSaveUri);
                    if(outputStream!=null){
                        mResultBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
                        outputStream.close();
                        StaticBitmapHolder.setFinaldata(mSaveUri);
                         Intent publish = new Intent(FilterActivity.this, PublishFragment.class);
                         startActivity(publish);

                    }

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                // Intent publish = new Intent(FilterActivity.this, PublishFragment.class);
                    //StaticBitmapHolder.setFinaldata(data);

                    //Nullify the mResultBitmap we dont need it any more !!
                    //mResultBitmap=null; --> These 2 lines should be called from onDEstroy
                    //System.gc();    --> Adn this also
                    //pdialog.dismiss();
                   // startActivity(publish);

            }
            });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //go back to the Gallery Fragment
            }
        });
    }
    private int calculateBitmapSampleSize(Uri bitmapUri) throws IOException {
        InputStream is = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            is = getContentResolver().openInputStream(bitmapUri);
            BitmapFactory.decodeStream(is, null, options); // Just get image size
        } finally {
            CropUtil.closeSilently(is);
        }

        int maxSize = getMaxImageSize();
        int sampleSize = 1;
        while (options.outHeight / sampleSize > maxSize || options.outWidth / sampleSize > maxSize) {
            sampleSize = sampleSize << 1;
        }
        return sampleSize;
    }
    private int getMaxImageSize() {
        int textureLimit = getMaxTextureSize();
        if (textureLimit == 0) {
            return SIZE_DEFAULT;
        } else {
            return Math.min(textureLimit, SIZE_LIMIT);
        }
    }

    private int getMaxTextureSize() {
        // The OpenGL texture size is the maximum size that can be drawn in an ImageView
        int[] maxSize = new int[1];
        GLES10.glGetIntegerv(GLES10.GL_MAX_TEXTURE_SIZE, maxSize, 0);
        return maxSize[0];
    }





    public List<Filter> returnList(){
        List<Filter>temp=new ArrayList<Filter>();
        for(int i=0;i<4;i++){
            temp.add(new Filter(computeFilter(i),String.valueOf(FiltersArray.availableFilters[i]),i));
        }
        return temp;
    }
    //Idiotic Way of Async Data Loading
    public  void Additionalcompute1(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for(int i=4;i<myfilters.length;i++){
                            filtersAdapter.getList().add(new Filter(computeFilter(i), String.valueOf(FiltersArray.availableFilters[i]), i));
                            filtersAdapter.notifyDataSetChanged();
                            recList.scrollBy(1, 1);
                        }
                        Log.d("Filter Activity","Finished Loading");
                        loading.dismiss();
                    }
                });
            }
        }).start();
    }


    public Bitmap computeFilter(int position){
        ICVFilter current=myfilters[position];
        ImageData d = current.convert(mImageData);
        d.createResult();
        Bitmap mResultBitmap = d.getResult();
        //Breaking teh Encapsulation  :D : D
        d=null;
        System.gc();
        return mResultBitmap;
    }

    @Override
    public void ApplyFilter(int filter) {
        ICVFilter current=myfilters[filter];
        ImageData bigfilter = new ImageData(orginalBitmap);
        ImageData d = current.convert(bigfilter);
        d.createResult();
        mImageData=null;
        System.gc();
        mResultBitmap = d.getResult();
        filterpreview.setImageBitmap(mResultBitmap);
    }


    public boolean handleMessage(Message msg) {
        Log.d("Message", "Handled");
        return  true;
    }

    private Bitmap returnSampledBitmap(){
        InputStream in = null;
        Bitmap returnedBitmap = null;
        try {
           // Log.d("ImageUri",StaticBitmapHolder.getCameraimageUri().getPath());
            in = getContentResolver().openInputStream(StaticBitmapHolder.getCroppedimageUri());
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();
            int scale = 1;
            if (o.outHeight > filterpreview.getHeight() || o.outWidth > filterpreview.getWidth()) {
                scale = (int) Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            Log.d("Sample Vaue",String.valueOf(scale));
            in = getContentResolver().openInputStream(StaticBitmapHolder.getCroppedimageUri());
            returnedBitmap = BitmapFactory.decodeStream(in, null, o2);
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Decode image size
        return returnedBitmap;

    }

    private Bitmap returnSamllestBitmap(){
        InputStream in = null;
        Bitmap returnedBitmap = null;
        try {
            in = getContentResolver().openInputStream(StaticBitmapHolder.getCroppedimageUri());
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();
            int scale = 1;
            if (o.outHeight > 100 || o.outWidth > 100) {//100 * 100 is the stuff na !!
                scale = (int) Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            in = getContentResolver().openInputStream(StaticBitmapHolder.getCroppedimageUri());
            returnedBitmap = BitmapFactory.decodeStream(in, null, o2);
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Decode image size
        return returnedBitmap;

    }
}
