package com.nepalimutu.pujanpaudel.app.priceoverrrflow.FragmentsCorner;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Adapters.FiltersAdapter;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Adapters.PhotoFeedAdapter;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Models.Feed;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Models.Filter;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Models.LoadListener;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.ParseModels.ParsePost;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by pujan paudel on 11/3/2015.
 */
public class FeedFragment extends Fragment {
    private RecyclerView recList;
    private PhotoFeedAdapter feedAdapter;
    private List<ParsePost> mphotos = new ArrayList<ParsePost>();
    private List<LoadListener>mloaders=new ArrayList<LoadListener>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.feedfragment, container, false);
        recList = (RecyclerView) view.findViewById(R.id.postsRecycler);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);
        feedAdapter = new PhotoFeedAdapter(mphotos,mloaders);
        recList.setAdapter(feedAdapter);
        returnList();
        return view;
    }

    public void  returnList() {
        ParseQuery<ParsePost> query = ParsePost.returnQuery();
        query.findInBackground(new FindCallback<ParsePost>() {
            @Override
            public void done(List<ParsePost> list, ParseException e) {

                if (e == null) {
                    //No problem in loading the dataa

                        for (ParsePost post : list) {
                            mphotos.add(post);
                            mloaders.add(new LoadListener(post.getPicture().getUrl(),false,0));
                        }
                    feedAdapter.notifyDataSetChanged();
                    feedAdapter.beginLoading();
                    recList.scrollBy(1,1);
                } else {
                    e.printStackTrace();
                    //We have so much of a custom logic thing Here !!!  :D
                }
            }
        });
    }

}
