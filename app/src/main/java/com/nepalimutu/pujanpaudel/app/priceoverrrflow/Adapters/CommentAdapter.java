package com.nepalimutu.pujanpaudel.app.priceoverrrflow.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Models.Comments;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.R;

import java.util.List;

/**
 * Created by pujan paudel on 11/21/2015.
 */
public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentViewHolder> {

    private List<Comments> mComments;
    private Context ctx;

    public CommentAdapter(List<Comments>list){
        mComments=list;
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ctx=parent.getContext();
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_comment, parent, false);
        return new CommentViewHolder(itemView);    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, int position) {

        Comments curr_comment=mComments.get(position);
        holder.commentor.setText(curr_comment.commentor);
        holder.comment.setText(curr_comment.comment);

        int flags = DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME|  DateUtils.FORMAT_NO_YEAR;
        long millisecond =curr_comment.comment_date.getTime();
        String monthAndDayText = DateUtils.formatDateTime(ctx, millisecond, flags);

        holder.commentdate.setText(monthAndDayText);
        //Leave the Comment Emoticon Blank for now


    }

    @Override
    public int getItemCount() {
        return mComments.size();  //-> Leave the Footer for Now , Now No See More  For See more add +1
    }

    public static class CommentViewHolder extends RecyclerView.ViewHolder{
       public TextView commentor;

        public TextView comment;
        public ImageView emoticon;
        public TextView commentdate;
        public View currentview;

        public CommentViewHolder(View v) {
                super(v);
            currentview=v;
            commentor=(TextView)v.findViewById(R.id.commentor);
            comment=(TextView)v.findViewById(R.id.comment);
            emoticon=(ImageView)v.findViewById(R.id.emoticon_gif);
            commentdate=(TextView)v.findViewById(R.id.comment_time);



        }

    }

}
