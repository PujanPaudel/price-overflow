package com.nepalimutu.pujanpaudel.app.priceoverrrflow.Models;

import java.util.Date;

/**

 * Created by pujan paudel on 11/9/2015.
 */
public class Feed {
    public Feed(String poster, Date posteddate, String caption, int likes) {
        this.poster = poster;
        this.posteddate = posteddate;
        this.caption = caption;
        this.likes = likes;
    }

    private Date posteddate;
    private String poster;
    //private Bitmap sourceimage;
    private String caption;
    private int likes;

    public String getPoster() {
        return poster;
    }

    public String getCaption() {
        return caption;
    }
    public int getLikes(){
        return likes;
    }
    public Date getDate(){
        return posteddate;
    }
}
