package com.nepalimutu.pujanpaudel.app.priceoverrrflow.Models;

import android.graphics.Bitmap;

/**
 * Created by pujan paudel on 11/20/2015.
 */

public class LoadListener {
    private Bitmap finalbmp;

    public Bitmap getFinalbmp() {
        return finalbmp;
    }

    public int getProgressvalue() {
        return progressvalue;
    }

    public boolean isComplete() {
        return complete;
    }

    public String getMyurl() {
        return myurl;
    }

    public void setFinalbmp(Bitmap finalbmp) {
        this.finalbmp = finalbmp;
    }

    public LoadListener(String myurl, boolean complete, int progressvalue) {

        this.myurl = myurl;
        this.complete = complete;
        this.progressvalue = progressvalue;
    }

    private int progressvalue;
    private boolean complete;
    private String myurl;

    public void setProgressvalue(int progressvalue) {
        this.progressvalue = progressvalue;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }

    public void setMyurl(String myurl) {
        this.myurl = myurl;
    }
}
