package com.nepalimutu.pujanpaudel.app.priceoverrrflow.Models;

import android.graphics.Bitmap;

/**
 * Created by pujan paudel on 11/5/2015.
 */
public class Filter {
    public Bitmap getPreview() {
        return preview;
    }

    public int getFilterid() {
        return filterid;
    }

    public void setFilterid(int filterid) {
        this.filterid = filterid;
    }

    public Filter(Bitmap preview, String filtername,int filterid) {
        this.preview = preview;
        this.filtername = filtername;
        this.filterid=filterid;

    }

    public void setPreview(Bitmap preview) {
        this.preview = preview;
    }

    public String getFiltername() {
        return filtername;
    }

    public void setFiltername(String filtername) {
        this.filtername = filtername;
    }

    private Bitmap preview;
    private String filtername;
    private int filterid;
}
