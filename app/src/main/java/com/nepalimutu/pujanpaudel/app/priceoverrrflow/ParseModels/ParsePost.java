package com.nepalimutu.pujanpaudel.app.priceoverrrflow.ParseModels;

import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONArray;

/**
 * Created by pujan paudel on 11/19/2015.
 */
@ParseClassName("Post")
public class ParsePost extends ParseObject {

    /**
     * @Params of which we are going to derive the model
     * Poster(pointer)
     * PosterName(String)
     * Picture(File)
     * Caption(String)
     * HashTags(String Array)
     * Favourites(Number)
     * Favourites Array(Array)
     *TargetAudience (Array)
     */

    public ParseUser getPoster(){
        return getParseUser("Poster");
    }

    public void setPoster(ParseUser poster){
        put("Poster",poster);
    }

    public String getPosterName(){
        return getString("PosterName");
    }

    public void setPosterName(String poster){
        put("PosterName",poster);
    }
    public   void setPicture(ParseFile picture){
        put("Picture",picture);
    }

    public ParseFile getPicture(){
        return getParseFile("Picture");
    }

    public String getCaption(){
        return getString("Caption");
    }
    public void setCaption(String caption){
        put("Caption",caption);
    }

    public JSONArray getHashTags(){
        return getJSONArray("HashTags");
    }
    public void setHashTags(JSONArray hashTags){
        put("HashTags",hashTags);
    }

    public void setFavourites(int number){
        put("Favourites",number);
    }

    public int getFavourites(){
        return getInt("Favourites");
    }

    public void setFavouritesArray(JSONArray favouritesArray){
        put("FavouritesArray",favouritesArray);
    }
    public JSONArray getFavouritesArray(){
        return getJSONArray("FavouritesArray");
    }

    public JSONArray getTargetAudience(){
        return getJSONArray("TargetAudience");
    }
    public void setTargetAudience(JSONArray target){
        put("TargetAudience",target);
    }


    public void setPictureData(byte[]data){
        put("Data",data);
    }
    //No Get TempData

    public static ParseQuery<ParsePost>returnQuery(){
        return ParseQuery.getQuery(ParsePost.class);
    }
}
