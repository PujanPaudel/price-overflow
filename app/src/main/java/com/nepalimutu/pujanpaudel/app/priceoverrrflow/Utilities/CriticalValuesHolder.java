package com.nepalimutu.pujanpaudel.app.priceoverrrflow.Utilities;

/**
 * Created by pujan paudel on 11/20/2015.
 */
public class CriticalValuesHolder {
    private static int croppedimagewidth;

    public static int getCroppedimageheight() {
        return croppedimageheight;
    }

    public static void setCroppedimageheight(int croppedimageheight) {
        CriticalValuesHolder.croppedimageheight = croppedimageheight;
    }

    public static int getCroppedimagewidth() {
        return croppedimagewidth;
    }

    public static void setCroppedimagewidth(int croppedimagewidth) {
        CriticalValuesHolder.croppedimagewidth = croppedimagewidth;
    }

    private static int croppedimageheight;
}
