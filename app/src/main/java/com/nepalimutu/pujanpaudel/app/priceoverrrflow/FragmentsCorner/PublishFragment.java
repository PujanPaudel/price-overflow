package com.nepalimutu.pujanpaudel.app.priceoverrrflow.FragmentsCorner;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.ViewSwitcher;

import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Adapters.HashTagRecycler;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.ParseModels.PostFactory;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.R;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Utilities.StaticBitmapHolder;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.ProgressCallback;
import com.todddavies.components.progressbar.ProgressWheel;

import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import cn.finalteam.toolsfinal.DeviceUtils;

/**
 * Created by pujan paudel on 11/15/2015.
 */
public class PublishFragment extends AppCompatActivity implements View.OnClickListener {

    private EditText caption;
    private EditText hashtag;
    private ImageView finalImage;//final Image name is confusing , actually its a scaled image
    private ToggleButton facebookshare;
    private ToggleButton twittershare;
    private ToggleButton linkedinshare;
    private ToggleButton followers;
    private Button publish;
    private Bitmap scaledBitmap;
    private Bitmap originalBitmap;
    private TextSwitcher textCount;
    private int totalCount=140;
    private ProgressWheel saving_image;
    private final int IMAGE_MAX_SIZE = 1024;
    private CardView contentcard;
    private ParseFile imagefile;
    public static PublishFragment reference;
    private byte[]filedata;
    private RecyclerView hashrec;
    private HashTagRecycler adapter;
    private List<String> hashes=new ArrayList<String>();
    //Switcher Factory !! Hmm  Give a think
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reference=this;
        setContentView(R.layout.activity_publish);
        caption=(EditText)findViewById(R.id.Description);
        hashtag=(EditText)findViewById(R.id.hashtag);
        textCount=(TextSwitcher)findViewById(R.id.wordsCount);
        contentcard=(CardView)findViewById(R.id.card_view);
        finalImage=(ImageView)findViewById(R.id.ivPhoto); //Clicking the Image Should give a view of the Full Image
        saving_image=(ProgressWheel)findViewById(R.id.pw_spinner);
        /** Just keep this
         * thing in mind, either implement thi sin an activity or a fragment
         */

        hashrec=(RecyclerView)findViewById(R.id.hashtags);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        hashrec.setLayoutManager(llm);
        hashrec.setHasFixedSize(true);
        adapter=new HashTagRecycler(hashes);
        hashrec.setAdapter(adapter);
        finalImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm=getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.previewfragment,new LargePreviewFragment(originalBitmap,caption.getText().toString())).addToBackStack(null).commit();
            }
        });
        facebookshare=(ToggleButton)findViewById(R.id.toggleFacebook);
        twittershare=(ToggleButton)findViewById(R.id.toggleTwitter);
        linkedinshare=(ToggleButton)findViewById(R.id.toggleLinkedin);
        followers=(ToggleButton)findViewById(R.id.tbFollowers);
        publish=(Button)findViewById(R.id.done_publish);
        setSwitcher();
        setListeners();
        //runOnUiThread(new Runnable() {
  //          @Override
           // public void run() {
         //       setPreviewBitmap();
          //      saveImageInBackGround();


            //}
//        });
    }

    private void setPreviewBitmap(){
        //Fade -> Alpha
        finalImage.setAlpha(0.1f);
        originalBitmap=returnSampledBitmap(contentcard.getWidth(),contentcard.getHeight());
        scaledBitmap=Bitmap.createScaledBitmap(originalBitmap,96,96,true);
        finalImage.setImageBitmap(scaledBitmap);
    }

    private void setSwitcher(){
        textCount.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                TextView myText = new TextView(PublishFragment.this);
                myText.setGravity(Gravity.TOP | Gravity.RIGHT);
                myText.setTextSize(18);
                myText.setTextColor(Color.BLUE);
                return myText;
            }
        });
        Animation in = AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left);
        Animation out = AnimationUtils.loadAnimation(this,android.R.anim.slide_out_right);
        // set the animation type of textSwitcher
        textCount.setInAnimation(in);
        textCount.setOutAnimation(out);
    }


    private void setListeners(){

        caption.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //i2
                textCount.setText(String.valueOf(totalCount - i2));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        publish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //We need our custom Loading Dialog
                JSONArray hashtags=new JSONArray();
               for(String Hash:hashes){
                   hashtags.put(Hash);
               }
                PostFactory pf = new PostFactory(ParseUser.getCurrentUser(), "Pujan Paudel", imagefile, caption.getText().toString(),hashtags, 0, new JSONArray(), new JSONArray(), filedata);
                pf.Parcel();
            }
        });



        hashtag.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

                if (charSequence.toString().endsWith(" ")) {
                    Log.i("Fired", "hashtag");
                    String hash = hashtag.getText().toString().trim();
                    hashes.add(hash);
                    hashtag.setText("");
                    hashtag.clearComposingText();
                    hashtag.requestFocus();
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.done_publish:
                //Do the Task of Network Request
            case R.id.toggleFacebook:
                //Similary Assign Task for evrything
            default:
        }
    }

    private void saveImageInBackGround(){

        ByteArrayOutputStream bos=new ByteArrayOutputStream();
        originalBitmap.compress(Bitmap.CompressFormat.JPEG,100,bos);
        filedata=bos.toByteArray();
         imagefile=new ParseFile(filedata);
        imagefile.saveInBackground(new ProgressCallback() {
            @Override
            public void done(Integer integer) {
                Log.i("Progress Value", String.valueOf(Math.ceil(integer * 3.6)));
                saving_image.setProgress(Math.round(integer * 3.6f));
                finalImage.setAlpha(integer * 0.1f);
            }
        });
    }


    private Bitmap returnSampledBitmap(int width,int height){
        InputStream in = null;
        Bitmap returnedBitmap = null;
        try {
            // Log.d("ImageUri",StaticBitmapHolder.getCameraimageUri().getPath());
            in = getContentResolver().openInputStream(StaticBitmapHolder.getFinalUri());
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();
            int scale = 1;
            if (o.outHeight >height || o.outWidth >width) {
                scale = (int) Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            Log.d("Sample Vaue",String.valueOf(scale));
            in = getContentResolver().openInputStream(StaticBitmapHolder.getCroppedimageUri());
            returnedBitmap = BitmapFactory.decodeStream(in, null, o2);
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Decode image size
        return returnedBitmap;

    }


}
