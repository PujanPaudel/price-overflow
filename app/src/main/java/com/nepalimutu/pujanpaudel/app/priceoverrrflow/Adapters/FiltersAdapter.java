package com.nepalimutu.pujanpaudel.app.priceoverrrflow.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nepalimutu.pujanpaudel.app.priceoverrrflow.FilterActivity;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Models.Filter;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pujan paudel on 11/5/2015.
 */
public class FiltersAdapter extends RecyclerView.Adapter<FiltersAdapter.FiltersViewHolder>{

    private List<Filter> filtersLisr;
    private Context ctx;
    private List<FiltersViewHolder> myholders=new ArrayList<FiltersViewHolder>();

    public FiltersAdapter(List<Filter>mList){
        filtersLisr=mList;
    }
    @Override
    public FiltersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ctx=parent.getContext();
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.filterlayout, parent, false);
        return new FiltersViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FiltersViewHolder holder, int position) {
        myholders.add(holder);
        final Filter current=filtersLisr.get(position);
        holder.currentview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FilterActivity.reference.ApplyFilter(current.getFilterid());
            }
        });
        holder.filtername.setText(current.getFiltername());

        holder.filterpreview.setImageBitmap(current.getPreview());
      //  current.getPreview().recycle();
        //Do the same for Filterbmp Dude :D
    }

    @Override
    public int getItemCount() {
        return filtersLisr.size();
    }

    public static class FiltersViewHolder extends RecyclerView.ViewHolder{
        public ImageView filterpreview;
        private TextView filtername;
        public View currentview;
        public FiltersViewHolder(View v) {
            super(v);
            currentview=v;
           filterpreview=(ImageView)v.findViewById(R.id.filterpreview);
            filtername=(TextView)v.findViewById(R.id.filtertype);
        }

    }
    public  List<Filter> getList(){
        return filtersLisr;
    }

    public List<FiltersViewHolder>getHolders(){
        return myholders;
    }
}
