package com.nepalimutu.pujanpaudel.app.priceoverrrflow;

import android.content.Context;
import android.os.Bundle;

import com.nepalimutu.pujanpaudel.app.priceoverrrflow.ParseModels.ParsePost;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.ParseModels.PostComment;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by pujan paudel on 10/31/2015.
 */
public class Application extends android.app.Application {

    @Override
    public void onCreate(){
        super.onCreate();
        Parse.initialize(this, "huNNLgqpZQeOn0f4s0Zr8Geb9247M12nYOKDbNRp", "TmzyBomokSA0XR07fstJIVOYAnbPk7f9HjXTbuTP");
        ParseUser.enableAutomaticUser();
        ParseACL defaultACL=new ParseACL();
        defaultACL.setPublicReadAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);
        ParseObject.registerSubclass(ParsePost.class);
        ParseObject.registerSubclass(PostComment.class);
            // This configuration tuning is custom. You can tune every option, you may tune some of them,
            // or you can create default configuration by
            //  ImageLoaderConfiguration.createDefault(this);
            // method.
            ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(getApplicationContext());
            config.threadPriority(Thread.NORM_PRIORITY - 2);
            config.denyCacheImageMultipleSizesInMemory();
        config.memoryCache(new WeakMemoryCache());
            config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
            config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
            config.tasksProcessingOrder(QueueProcessingType.FIFO);
            config.writeDebugLogs(); // Remove for release app
            // Initialize ImageLoader with configuration.

            ImageLoader.getInstance().init(config.build());
        }

}
