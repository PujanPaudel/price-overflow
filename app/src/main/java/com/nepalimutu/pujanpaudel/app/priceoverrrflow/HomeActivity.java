package com.nepalimutu.pujanpaudel.app.priceoverrrflow;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;

import com.astuetz.PagerSlidingTabStrip;
import com.cn.finalteam.galleryfinal.GalleryHelper;
import com.cn.finalteam.galleryfinal.PhotoChooseActivity;
import com.com.desmond.squarecamera.CameraActivity;
import com.com.desmond.squarecamera.CameraFragment;
import com.com.fenchtose.touch2focus.TouchActivity;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.FragmentsCorner.FeedFragment;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.FragmentsCorner.BaseImagesContainer;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.FragmentsCorner.PhotoGalleryFragment;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.FragmentsCorner.PublishFragment;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.FragmentsCorner.UpdatedGallery;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

private ViewPager viewPager;
    private PagerSlidingTabStrip strip;
    public static  HomeActivity reference;
    private Toolbar mytoolbar;
        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            reference=this;
        setContentView(R.layout.activity_home);
            mytoolbar=(Toolbar)findViewById(R.id.toolbar);
            setSupportActionBar(mytoolbar);
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, mytoolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);

            FloatingActionButton newImage=(FloatingActionButton)findViewById(R.id.new_photo);
           // newImage.setOnClickListener(new View.OnClickListener() {
             //   @Override
               // public void onClick(View view) {
                 //   Intent intent=new Intent(HomeActivity.this,BaseImagesContainer.class);
                  //  startActivity(intent);

               // }
            //});
            newImage.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                 Intent intent=new Intent(HomeActivity.this,UpdatedGallery.class); //UpdatedGallery.classs
                    startActivity(intent);
//                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_in_right);

                   // Intent startCustomCameraIntent = new Intent(HomeActivity.this, CameraActivity.class);
                    //startActivityForResult(startCustomCameraIntent, 300);
                    return false;
                }
            });
            FragmentManager manager=getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.container,new FeedFragment()).commit();

        }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);        }
        //Intent startCustomCameraIntent = new Intent(this, CameraActivity.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle s bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;

        if (requestCode == 300) {
            Uri photoUri = data.getData();
            Log.d("Uri", String.valueOf(photoUri.getPath()));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}