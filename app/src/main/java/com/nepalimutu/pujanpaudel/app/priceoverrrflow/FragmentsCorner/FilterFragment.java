package com.nepalimutu.pujanpaudel.app.priceoverrrflow.FragmentsCorner;

import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Adapters.FiltersAdapter;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Models.Filter;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pujan paudel on 11/5/2015.
 */
public class FilterFragment extends Fragment {
    private RecyclerView recList;
    private FiltersAdapter filtersAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.filterfragment, container, false);
recList=(RecyclerView)view.findViewById(R.id.recyclerfilter);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        recList.setLayoutManager(llm);
        filtersAdapter=new FiltersAdapter(returnList());
        recList.setAdapter(filtersAdapter);
        return view;
    }

    public List<Filter> returnList(){
        List<Filter>temp=new ArrayList<Filter>();
        for(int i=0;i<15;i++){
            temp.add(new Filter(null,"Filter "+ String.valueOf(i),i));
        }
        return temp;
    }
}
