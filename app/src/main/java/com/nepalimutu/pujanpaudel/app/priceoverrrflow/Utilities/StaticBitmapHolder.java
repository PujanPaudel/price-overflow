package com.nepalimutu.pujanpaudel.app.priceoverrrflow.Utilities;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by pujan paudel on 11/18/2015.
 */
public class StaticBitmapHolder {

    private static Bitmap finalBitmap;
    private static Uri cameraimageUri;
    private static Uri croppedimageUri;
    private static Uri finalimageUri;

    public static Uri getFinalUri() {
        return finalimageUri;
    }

    public static void setFinaldata(Uri finaldata) {
        StaticBitmapHolder.finalimageUri = finaldata;
    }

    public static Uri getCroppedimageUri() {
        return croppedimageUri;
    }

    public static void setCroppedimageUri(Uri croppedimageUri) {
        StaticBitmapHolder.croppedimageUri = croppedimageUri;
    }

    private static Bitmap croppedBitmap;

    public static Bitmap getCroppedBitmap() {
        return croppedBitmap;
    }

    public static void setCroppedBitmap(Bitmap croppedBitmap) {
        StaticBitmapHolder.croppedBitmap = croppedBitmap;
    }

    public static Uri getCameraimageUri() {
        return cameraimageUri;

    }

    public static void setCameraimageUri(Uri cameraimageUri) {
        StaticBitmapHolder.cameraimageUri = cameraimageUri;
    }

    public static Bitmap getFinalBitmap() {
        return finalBitmap;
    }

    public static void setFinalBitmap(Bitmap bitmap) {
        finalBitmap = bitmap;
    }
}
