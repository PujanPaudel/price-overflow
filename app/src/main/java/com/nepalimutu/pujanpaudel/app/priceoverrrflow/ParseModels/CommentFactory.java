package com.nepalimutu.pujanpaudel.app.priceoverrrflow.ParseModels;

import android.app.ProgressDialog;
import android.content.Context;

import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Application;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

/**
 * Created by pujan paudel on 11/20/2015.
 */
public class CommentFactory {

    /**
     *
     * @param photo
     * @paramcommentor
     * @paramcomment
     * @paramemoticindex
     * @paracommentorname
     */

    private ParsePost parentpost;
    private ParseUser commentor;
    private String commentorname;
    private String comment;
    private int emoindex;
    private Context context;

    public CommentFactory(ParsePost parentpost, ParseUser commentor, String commentorname, String comment, int commentindex,Context ctx) {
        this.parentpost = parentpost;
        this.commentor = commentor;
        this.commentorname = commentorname;
        this.comment = comment;
        this.emoindex = commentindex;
    }

    public void Parcel(){
        final ProgressDialog progressDialog=new ProgressDialog(context);
        progressDialog.setMessage("Posting Comment");
        progressDialog.show();
        PostComment pc=new PostComment();
        pc.setComment(comment);
        pc.setCommentorName(commentorname);
        pc.setCommentor(commentor);
        pc.setEmoticIndex(emoindex);
        pc.setPhoto(parentpost);
        ParseACL acl=new ParseACL();
        acl.setPublicReadAccess(true);
        acl.setPublicWriteAccess(true);
        pc.setACL(acl);
        pc.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if(e==null){
                    progressDialog.dismiss();
                }else{
                    e.printStackTrace();
                }
            }
        });

    }
}
