package com.nepalimutu.pujanpaudel.app.priceoverrrflow.FragmentsCorner;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Toast;

import com.com.desmond.squarecamera.CameraFragment;
import com.com.fenchtose.touch2focus.TouchActivity;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.FilterActivity;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.HomeActivity;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.R;
import com.soundcloud.android.crop.Crop;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pujan paudel on 11/3/2015.
 */
public class BaseImagesContainer extends AppCompatActivity {
    private ViewPager viewPager;
    public static BaseImagesContainer reference;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.picture_take_activity);
        viewPager =(ViewPager)findViewById(R.id.tabanim_viewpager);
        setupViewPager(viewPager);
        reference=this;
    }
    private void setupViewPager(final ViewPager viewPager) {
        Log.d("Set Up ", "View Pager");
        final ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new PhotoGalleryFragment(), "  Gallery    "); //This Very tHing is variable
       adapter.addFrag(new CameraFragment(),"Camera");
//        adapter.addFrag(new NativeCameraFragment(),"Camera");
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                // viewPager.getAdapter().notifyDataSetChanged();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
       // TabLayout tabLayout = (TabLayout)findViewById(R.id.tabanim_tabs);
       // tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        //tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);


//        tabLayout.setupWithViewPager(viewPager);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }
        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }
        @Override
        public int getCount() {
            return mFragmentList.size();
        }
        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public int getItemPosition(Object item){

            return POSITION_NONE;
        }
    }
    public void showToast(String message){
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent result) {
        Log.d("Cropped", "The Image");
        if(resultCode!=RESULT_CANCELED){
            handleCrop(resultCode, result);
        }else{
            //cancelled
           // Intent intent=new Intent(BaseImagesContainer.this,HomeActivity.class);
            //startActivity(intent);
            TouchActivity.reference.cropCancelled();
        }
    }
    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            Intent filter = new Intent(BaseImagesContainer.reference, FilterActivity.class);
            filter.putExtra("Bitmap", Crop.getOutput(result).getPath());
            startActivity(filter);

        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(getApplicationContext(), Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        } else if (resultCode == RESULT_CANCELED) {
            finish();
        }
    }

    @Override
    public void onBackPressed(){
        Log.d("BaseImagesContainer", "onBackPressed");
        Intent myintent=new Intent(BaseImagesContainer.this,HomeActivity.class);
        startActivity(myintent);

    }
}
