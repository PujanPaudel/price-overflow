package com.nepalimutu.pujanpaudel.app.priceoverrrflow.FragmentsCorner;

import android.annotation.SuppressLint;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.opengl.GLES10;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.com.Utilities.ImageCropFragment;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Interfaces.ResetCropCallback;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Interfaces.StopLoadingCallback;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.MyHorizontalLayout;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.R;
import com.soundcloud.android.crop.Crop;
import com.soundcloud.android.crop.CropImageView;
import com.soundcloud.android.crop.CropUtil;
import com.soundcloud.android.crop.HighlightView;
import com.soundcloud.android.crop.ImageViewTouchBase;
import com.soundcloud.android.crop.RotateBitmap;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

/**
 * Created by pujan paudel on 11/3/2015.
 */
public class PhotoGalleryFragment extends Fragment implements AdapterView.OnItemClickListener,StopLoadingCallback,ResetCropCallback{
    private Cursor cursor;
    private int columnIndex;
    private GridView sdcardImages;
    private ImageAdapter imageAdapter;
    private Display display;
    private RelativeLayout proceedlayout;
    private Bitmap currentbitmap;

    //The Image Cropping Part Goes Here My DUde !!
    private static final int SIZE_DEFAULT = 2048;
    private static final int SIZE_LIMIT = 4096;
    private boolean load=true;
    private final Handler handler = new Handler();

    private int aspectX;
    private int aspectY;
    int recycleCount=0;
    // Output image
    private int maxX=800;
    private int maxY=800;
    private int exifRotation;

    private Uri sourceUri;
    private Uri saveUri;

    private boolean isSaving;

    private int sampleSize;
    private RotateBitmap rotateBitmap;
    private HighlightView cropView;
    public static PhotoGalleryFragment reference;

    //End it

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        reference=this;
        display = ((WindowManager)getActivity().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.myhorizontallayout, container, false);
        proceedlayout=(RelativeLayout)view.findViewById(R.id.topbar);
        // Create the cursor pointing to the SDCard
        sdcardImages = (GridView) view.findViewById(R.id.sdcard);
        sdcardImages.setNumColumns(4);
        sdcardImages.setClipToPadding(false);
        imageAdapter = new ImageAdapter(getActivity().getApplicationContext());
        sdcardImages.setAdapter(imageAdapter);
        getActivity().setProgressBarIndeterminateVisibility(true);
        sdcardImages.setOnItemClickListener(this);


        //BUTTONS
        ImageButton forward=(ImageButton)view.findViewById(R.id.done);
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //preview is the imageview to go for
                Uri destination = Uri.fromFile(new File(getActivity().getCacheDir(), "cropped"));
                Uri source = getImageUri(getActivity().getApplicationContext(), currentbitmap);
                Crop.of(source, destination).withMaxSize(800, 800).start(getActivity());
            }
        });


        loadImages();
        return view;
    }

    /**

     * Adapter for our image files.

     */
    private void loadImages() {
        final Object data = getActivity().getLastNonConfigurationInstance();
        if (data == null) {
            new LoadImagesFromSDCard().execute();
        } else {
            final LoadedImage[] photos = (LoadedImage[]) data;
            if (photos.length == 0) {
                new LoadImagesFromSDCard().execute();
            }
            for (LoadedImage photo : photos) {
                addImage(photo);
            }
        }
    }

    private void addImage(LoadedImage... value) {
        for (LoadedImage image : value) {
            imageAdapter.addPhoto(image);
            imageAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void stopLoading() {
        Log.d("High Command","Stopped Loading");
        load=false;
    }

    @Override
    public void resetCrop(Uri uri) {
        FragmentManager fm=getChildFragmentManager();
        fm.beginTransaction().replace(R.id.previewfragment, new MyCropFragment(uri)).commit();
        fm.executePendingTransactions();

    }

    class LoadImagesFromSDCard extends AsyncTask<Object, LoadedImage, Object> {
        /**
         * Load images from SD Card in the background, and display each image on the screen.
         *
         * @
         */
        @Override
        protected Object doInBackground(Object... params) {
            //setProgressBarIndeterminateVisibility(true);
            Bitmap bitmap = null;
            Bitmap newBitmap = null;
            Uri uri = null;

            // Set up an array of the Thumbnail Image ID column we want
            String[] projection = {MediaStore.Images.Thumbnails._ID};
            // Create the cursor pointing to the SDCard
            Cursor cursor = getActivity().getContentResolver().query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI,
                    projection, // Which columns to return
                    null,       // Return all rows
                    null,
                    MediaStore.Images.Thumbnails.IMAGE_ID + " DESC");


            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails._ID);
            int size = cursor.getCount();
            // If size is 0, there are no images on the SD Card.
            if (size == 0) {
                //No Images available, post some message to the user
            }
            int imageID = 0;
            if(cursor.isClosed()){
                Log.d("Coool","I am Empty");
                return null;
            }
            for (int i = 0; i < size; i++) {
                if(load){
                    //continue;
                }else {
                    break;
                }
                cursor.moveToPosition(i);
                imageID = cursor.getInt(columnIndex);
                final String imagePath=cursor.getString(columnIndex);
                uri = Uri.withAppendedPath(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, "" + imageID);
                if(uri==null){
                    Log.d("URL ","Null");
                    return null;
                }

                if(getActivity()==null ||uri==null||getActivity().getContentResolver()==null){
                    Log.d("Content Resolver ","NULL");
                    return null;
                }
                if (uri != null) {
                    // if (bitmap != null) {
                    if(cursor.isFirst()) {
                        currentbitmap = bitmap;
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                SimulateTouch();
                            }
                        });
                    }
                    // Bitmap useThisBitmap = Bitmap.createScaledBitmap(bitmap, parent.getWidth(), parent.getHeight(), true);
                    publishProgress(new LoadedImage(bitmap,uri));

                }
                //}


            }
            cursor.close();
            return null;
        }
        /**
         * Add a new LoadedImage in the images grid.
         *
         * @param value The image.
         */
        @Override
        public void onProgressUpdate(LoadedImage... value) {
            addImage(value);
        }
        /**
         * Set the visibility of the progress bar to false.
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(Object result) {
            if(getActivity()!=null){
                getActivity().setProgressBarIndeterminateVisibility(false);
            }
        }
    }

    /**
     * Adapter for our image files.
     *
     * @author Mihai Fonoage
     *
     */
    class ImageAdapter extends BaseAdapter {

        private Context mContext;
        private ArrayList<LoadedImage> photos = new ArrayList<LoadedImage>();

        public ImageAdapter(Context context) {
            mContext = context;
        }

        public void addPhoto(LoadedImage photo) {
            photos.add(photo);
        }

        public int getCount() {
            return photos.size();
        }

        public Object getItem(int position) {
            return photos.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            final ImageView imageView;
            if (convertView == null) {
                imageView = new ImageView(mContext);
            } else {
                imageView = (ImageView) convertView;
            }

            Picasso.with(getActivity()).load(photos.get(position).getUri()).config(Bitmap.Config.RGB_565).centerCrop().resize(150,150).placeholder(R.drawable.placeholder).error(R.drawable.ic_launcher).into(imageView);
            // imageView.setImageBitmap(photos.get(position).getBitmap());
            return imageView;
        }
    }

    /**
     * A LoadedImage contains the Bitmap loaded for the image.
     */
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
    private static class LoadedImage {
        Bitmap mBitmap;
        Uri imagepath;

        LoadedImage(Bitmap bitmap,Uri myuri) {
            mBitmap = bitmap;
            imagepath=myuri;
        }

        public Bitmap getBitmap() {
            return mBitmap;
        }
        public  Uri getUri(){
            return imagepath;
        }
    }

    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
        int columnIndex = 0;
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI,
                projection,
                null,
                null,
                MediaStore.Images.Thumbnails.IMAGE_ID +" DESC");
        if (cursor != null) {
            columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToPosition(position);
            String imagePath = cursor.getString(columnIndex);
            FileInputStream is = null;
            BufferedInputStream bis = null;
            try {
                is = new FileInputStream(new File(imagePath));
                bis = new BufferedInputStream(is);
                Bitmap bitmap = BitmapFactory.decodeStream(bis);
                currentbitmap=bitmap;
                is.close();
                bis.close();

                FragmentManager fm=getChildFragmentManager();
                fm.beginTransaction().replace(R.id.previewfragment,new ImageCropFragment(Uri.fromFile(new File(imagePath)))).commit();
                //loadInput(Uri.fromFile(new File(imagePath)));
                //startCrop();
            }
            catch (Exception e) {
                //Try to recover
            }
            finally {
                try {
                    if (bis != null) {
                        bis.close();
                    }
                    if (is != null) {
                        is.close();
                    }
                    cursor.close();
                    projection = null;
                } catch (Exception e) {
                }
            }
        }
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        Log.d("I Finally","Destroyed Thyself");
        if (cursor!= null) {
            cursor.close();
            Log.d("Cursor Closed","Lol");
        }
        final GridView grid = sdcardImages;
        final int count = grid.getChildCount();
        ImageView v = null;
        for (int i = 0; i < count; i++) {
            v = (ImageView) grid.getChildAt(i);
            ((BitmapDrawable) v.getDrawable()).setCallback(null);
        }
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        try {
            bytes.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Uri.parse(path);
    }


//The ACtual Task for Cropping Goes Here

    public  void SimulateTouch(){
        int columnIndex = 0;
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI,
                projection,
                null,
                null,
                MediaStore.Images.Thumbnails.IMAGE_ID + " DESC");
        if (cursor != null) {
            columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToPosition(0);
            String imagePath = cursor.getString(columnIndex);
            FileInputStream is = null;
            BufferedInputStream bis = null;
            try {
                is = new FileInputStream(new File(imagePath));
                bis = new BufferedInputStream(is);
                Bitmap bitmap = BitmapFactory.decodeStream(bis);
                currentbitmap=bitmap;
                is.close();
                bis.close();

                FragmentManager fm=getChildFragmentManager();
                fm.beginTransaction().replace(R.id.previewfragment,new ImageCropFragment(Uri.fromFile(new File(imagePath)))).commit();
                fm.executePendingTransactions();
                //loadInput(Uri.fromFile(new File(imagePath)));
                //startCrop();
            }
            catch (Exception e) {
                //Try to recover
            }
            finally {
                try {
                    if (bis != null) {
                        bis.close();
                    }
                    if (is != null) {
                        is.close();
                    }
                    cursor.close();
                    projection = null;
                } catch (Exception e) {
                }
            }
        }

    }
}
