package com.nepalimutu.pujanpaudel.app.priceoverrrflow;
import android.app.Activity;

import android.content.Context;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import android.media.MediaScannerConnection;

import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;

import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;

import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.ButterKnife;

import butterknife.InjectView;

import com.astuetz.PagerSlidingTabStrip;

import com.squareup.picasso.Picasso.LoadedFrom;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import java.util.ArrayList;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageView;

public class Demo extends AppCompatActivity implements Target {

    private static final int MSG_SWITH_FILTER = 1001;
    private static final int MSG_SAVE_IMAGE = 1002;

    @InjectView(R.id.image) GPUImageView mImageView;

    int mFilter;
    int mWidth;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.new_post);
        ButterKnife.inject(this);

        WindowManager w = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        Display d = w.getDefaultDisplay();
        int rotation = d.getRotation();
        if (rotation == Surface.ROTATION_0 || rotation == Surface.ROTATION_180) {
            mWidth = d.getWidth();
        } else {
            mWidth = d.getHeight();
        }
        LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(mWidth, mWidth);
        mImageView.setLayoutParams(p);
        Picasso.with(this).load(R.drawable.hhkb).into(this);

    }

    public void onBitmapLoaded(Bitmap bitmap, LoadedFrom from) {
        mImageView.setImage(bitmap);
    }

    public void onBitmapFailed(Drawable errorDrawable) {
    }

    public void onPrepareLoad(Drawable placeHolderDrawable) {
    }

}