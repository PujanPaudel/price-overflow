package com.nepalimutu.pujanpaudel.app.priceoverrrflow.Utilities;

import android.content.Context;
import android.content.pm.PackageManager;

/**
 * Created by pujan paudel on 11/11/2015.
 */
public class FlashUtilities {

    public boolean isFlashAvailable(Context ctx){
        return ctx.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }
}
