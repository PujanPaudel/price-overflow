package com.nepalimutu.pujanpaudel.app.priceoverrrflow.Interfaces;

import android.net.Uri;

/**
 * Created by pujan paudel on 11/15/2015.
 */
public interface ResetCropCallback {
    public void resetCrop(Uri uri);
}
