package com.nepalimutu.pujanpaudel.app.priceoverrrflow.FragmentsCorner;

import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.cn.finalteam.galleryfinal.PhotoChooseActivity;
import com.cn.finalteam.galleryfinal.adapter.FolderListAdapter;
import com.cn.finalteam.galleryfinal.model.PhotoFolderInfo;
import com.com.Utilities.ImageCropFragment;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.Interfaces.ReadyForCropCallback;
import com.nepalimutu.pujanpaudel.app.priceoverrrflow.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pujan paudel on 11/17/2015.
 */
public class UpdatedGallery extends AppCompatActivity implements ReadyForCropCallback,View.OnClickListener, AdapterView.OnItemClickListener{
private FragmentManager fragmentmanager;
    public static UpdatedGallery reference;

    //Coming From Photo Choose Activity
    public ListView mLvFolderList;
    public LinearLayout mLllBottomBar;
    public LinearLayout mLlFolderPanel;
    public TextView mTvChooseFolderName;
    public static List<PhotoFolderInfo> mAllPhotoFolderList;
    public static FolderListAdapter mFolderListAdapter;
    private ImageButton done__crop;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.updatedgalleryview);
        mLvFolderList = (ListView)findViewById(R.id.lv_folder_list);
        mLllBottomBar = (LinearLayout)findViewById(R.id.ll_bottom_bar);
        mLlFolderPanel = (LinearLayout)findViewById(R.id.ll_folder_panel);
        mTvChooseFolderName = (TextView)findViewById(R.id.tv_choose_folder_name);
        done__crop=(ImageButton)findViewById(R.id.done_crop);
        mAllPhotoFolderList = new ArrayList<>();
        mFolderListAdapter = new FolderListAdapter(getApplicationContext(), mAllPhotoFolderList);
        mLvFolderList.setAdapter(mFolderListAdapter);
        setListener();

        fragmentmanager=getSupportFragmentManager();
        fragmentmanager.beginTransaction().replace(R.id.photo_grid,new PhotoChooseActivity()).commit();
        reference=this;
        done__crop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ImageCropFragment.reference.saveOutput();
            }
        });
    }
    private void setListener() {
        mLllBottomBar.setOnClickListener(this);
        mLvFolderList.setOnItemClickListener(this);
    }

    @Override
    public void Crop(String filepath) {
        fragmentmanager.beginTransaction().replace(R.id.cropfragment,new ImageCropFragment(Uri.fromFile(new File(filepath)))).commit();
        fragmentmanager.executePendingTransactions();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if ( id == R.id.ll_bottom_bar ) {
            if ( mLlFolderPanel.getVisibility() == View.VISIBLE ) {
                mLlFolderPanel.setVisibility(View.GONE);
                } else {
                Toast.makeText(getApplicationContext(),"Opening up ",Toast.LENGTH_LONG).show();
                mLlFolderPanel.setVisibility(View.VISIBLE);
            }
        }
    }

    //The Folder VIew onClick Listener
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        int parentId = parent.getId();
        if ( parentId == R.id.lv_folder_list ) {
            PhotoChooseActivity.reference.folderItemClick(position);
        }
    }


}
