package com.nepalimutu.pujanpaudel.app.priceoverrrflow.ParseModels;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by pujan paudel on 11/20/2015.
 */
@ParseClassName("Comment")
public class PostComment extends ParseObject{

    public PostComment(){
        super();
    }

    /**
     *
     * @param photo
     * @paramcommentor
     * @paramcommentorname
     * @paramemoticindex
     * @paracommentorname
     */
    public void setPhoto(ParsePost photo){
        put("Photo", photo);
    }

    public ParsePost getPhoto(){
        return (ParsePost)getParseObject("Photo");
    }
    public void setCommentor(ParseUser user){
        put("Commentor",user);
    }
    public ParseUser getCommentor(){
        return getParseUser("Commentor");
    }

    public void setComment(String comment){
        put("Comment",comment);
    }

    public String getComment(){
        return getString("Comment");
    }

    public void setEmoticIndex(int index){
        put("EmoIndex",index);
    }
    public  int getEmoticIndex(){
        return getInt("EmoIndex");
    }


    public void setCommentorName(String commentorName){
        put("CommentorName",commentorName);
    }

    public String getCommentorName(){
        return getString("CommentorName");
    }

    //Date not needed for now , Provided by parse Itself

}
